$(document).ready( function() {
    $('#fileupload').jquery.fileupload({
        maxChunkSize: $('#fileupload').data('junksize'),
        autoUpload:true

    }).on('fileuploadadd', function (e, data) {
        data.context = $('<div/>').appendTo('#bprs-fileupload');
        $.each(data.files, function (index, file) {
            var node = $('<p/>').append($('<span/>').text(file.name));
            node.appendTo(data.context);
        });
        $('#oktolab_intake_bundle_filetype_save').prop('disabled', true);
        data.submit();
        // add visual info for upload start
        $('#oktolab_intake_bundle_filetype_save').text($('#oktolab_intake_bundle_filetype_save').data('uploading'));
        $('#oktolab_intake_bundle_filetype_save').prop('disabled', true);

    }).on('fileuploadprogressall', function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#progress .progress-bar').css('width', progress + '%');

    }).on('fileuploadstop', function (e, data) {
        $('#oktolab_intake_bundle_filetype_save').prop('disabled', false);
    });
});