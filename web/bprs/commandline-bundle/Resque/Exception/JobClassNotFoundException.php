<?php

namespace Bprs\CommandLineBundle\Resque\Exception;

use Bprs\CommandLineBundle\Resque\ResqueException;

/**
 * Indicates that the job class could not be found
 */
class JobClassNotFoundException extends ResqueException
{
}
