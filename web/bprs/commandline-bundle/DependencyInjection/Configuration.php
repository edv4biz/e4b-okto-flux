<?php

namespace Bprs\CommandLineBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
	    $rootNode = $treeBuilder->root('bprs_command_line');
	    $rootNode
		    ->children()
		    ->scalarNode('php_path')->defaultValue('php')->end()
		    ->scalarNode('worker_queue')->defaultValue('*')->end()
		    ->scalarNode('redis_backend')->defaultValue('tcp://localhost:6379')->end()
		    ->scalarNode('bin_dir')->defaultValue('/../bin')->end()
		    ->end();
	    /* if (\method_exists($treeBuilder, 'getRootNode')) {
			 $rootNode = $treeBuilder->getRootNode();
			 $rootNode
				 ->children()
				 ->scalarNode('php_path')->defaultValue('php')->end()
				 ->scalarNode('worker_queue')->defaultValue('*')->end()
				 ->scalarNode('redis_backend')->defaultValue('tcp://localhost:6379')->end()
				 ->scalarNode('bin_dir')->defaultValue('/../bin')->end()
				 ->end();
		 } else {
			 // BC layer for symfony/config 4.1 and older
			 $rootNode = $treeBuilder->root('fos_message');
			 $rootNode
				 ->children()
				 ->scalarNode('php_path')->defaultValue('php')->end()
				 ->scalarNode('worker_queue')->defaultValue('*')->end()
				 ->scalarNode('redis_backend')->defaultValue('tcp://localhost:6379')->end()
				 ->scalarNode('bin_dir')->defaultValue('/../bin')->end()
				 ->end();
		 }*/


        return $treeBuilder;
    }
}
