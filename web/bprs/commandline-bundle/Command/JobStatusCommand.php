<?php

namespace Bprs\CommandLineBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

class JobStatusCommand extends ContainerAwareCommand
{
    private $jobservice;
    private $worker_queue;

    public function __construct($jobService, $worker_queue)
    {
        $this->jobservice = $jobService;
        $this->worker_queue = $worker_queue;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('bprs:commandline:status_job')
            ->setDescription('returns status of job in a given queue')
            ->addArgument('token', InputOption::VALUE_REQUIRED, 'Unique ID of job')
            ->addArgument('queue', InputOption::VALUE_REQUIRED, 'name of queue');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $token = $input->getArgument("token");
        $queue = $input->getArgument("queue", $this->worker_queue);
        
        $output->writeln($this->jobservice->getJobStatus($queue, $token));
    }
}
