<?php

namespace Bprs\CommandLineBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class RemoveJobCommand extends ContainerAwareCommand
{
    private $jobservice;
    private $worker_queue;

    public function __construct($jobService, $worker_queue)
    {
        $this->jobservice = $jobService;
        $this->worker_queue = $worker_queue;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('bprs:commandline:remove_job')
            ->setDescription('Removes job on given queue [index]')
            ->addOption('queue', null, InputOption::VALUE_REQUIRED, 'name of queue', $this->worker_queue)
            ->addOption('index', null, InputOption::VALUE_REQUIRED, 'job index')
            ->addOption('force', null, InputOption::VALUE_NONE, 'really remove job(s)')
            ->addOption('all', null, InputOption::VALUE_NONE, 'all jobs in queue');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $queue = $input->getOption('queue');
        $index = $input->getOption('index');

        if($input->getOption('force')) {
            if ($input->getOption('all')) {
                $this->jobservice->clearQueue($queue);
                $output->writeln(sprintf('Removed all Jobs in QUEUE [%s]', $queue));
            } else {
                $this->jobservice->removeJob($index, $queue);
                $output->writeln(sprintf('Removed Job [%s].', $index));
            }
        } else {
            $output->writeln(sprintf('Remove Job [%s]? Use --force', $index));
        }
    }
}
