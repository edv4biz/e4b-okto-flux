<?php

namespace Bprs\CommandLineBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\Table;

class ListJobsCommand extends ContainerAwareCommand
{
    private $jobservice;
    private $worker_queue;

    public function __construct($jobService, $worker_queue)
    {
        $this->jobservice = $jobService;
        $this->worker_queue = $worker_queue;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('bprs:commandline:list_jobs')
            ->setDescription('Lists all jobs on given queue (token)')
            ->addOption('queue', null, InputOption::VALUE_REQUIRED, 'name of queue', $this->worker_queue);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $queue = $input->getOption('queue');
        $jobs = $this->jobservice->listJobs($queue);
        $rows = [];
        foreach ($jobs as $index => $job) {
            $args = [];
            foreach ($job['args'] as $key => $value) {
                $args[] = $key.":".$value;
            }
            $rows[] = [$index, $job['id'], $job['class'], implode(", ", $args)];
        }

        $table = new Table($output);
        $table
            ->setHeaders(['number', 'id', 'class', 'args'])
            ->setRows($rows);

        $table->render();
    }
}
