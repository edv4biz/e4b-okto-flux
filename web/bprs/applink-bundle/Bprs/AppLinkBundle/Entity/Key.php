<?php

namespace Bprs\AppLinkBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;

/**
 * Key is the representation of the application (bundle) you want to share informations with
 *
 * @ORM\Table(name="table_key")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class Key
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
    * The application, bundle or action this key is for.
    * (for example: OktolabMediaBundle (OKTOLAB_MEDIA, OKTOLAB_MEDIA_SPECIALACTION), BprsUserBundle (BPRS_USER) )
    * @ORM\Column(name="role", type="string", length=32)
    */
    private $role;

    /**
     * @var \DateTime
     * When this key was created
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     * When this key was last updated
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
    * @ORM\ManyToOne(targetEntity="Keychain", inversedBy="keys")
    * @ORM\JoinColumn(name="keychain_id", referencedColumnName="id")
    */
    private $keychain;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     * @ORM\PrePersist
     * @param \DateTime $createdAt
     * @return App
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();

        return $this;
    }

    /**
     * Get createdAt
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     * @ORM\PrePersist
     * @ORM\PreUpdate
     * @param \DateTime $updatedAt
     * @return App
     */
    public function setUpdatedAt()
    {
        $this->updatedAt = new \DateTime();

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function getRole()
    {
        return $this->role;
    }

    public function setRole($role)
    {
        $this->role = $role;
        return $this;
    }

    public function setKeychain($keychain)
    {
        $this->keychain = $keychain;
        return $this;
    }

    public function getKeychain()
    {
        return $this->keychain;
    }
}
