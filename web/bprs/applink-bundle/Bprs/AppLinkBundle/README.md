# BprsAppLinkBundle
Allows CRUD linking of Applications 

If you want to allow different applications to share informations, you'll face the problem of securing those api's.  
With the BprsAppLinkBundle, controlling keys and permissions is easy.

#Installation

##Get Bundle
```
composer require bprs/applink-bundle
```

##Activate Bundle
```php
  //AppKernel
  new Bprs\AppLinkBundle\BprsAppLinkBundle(),
  new EightPoints\Bundle\GuzzleBundle\GuzzleBundle()
```
##Activate Routing
```yaml
bprs_applink:
    resource: .
    type: bprs_applink
```
##Set firewall, access_controls
```yaml
security:
    providers:
        bprs_applink:
            entity:
                class: BprsAppLinkBundle:Keychain
                property: user

    encoders:
        Bprs\AppLinkBundle\Entity\Keychain: plaintext

    firewalls:
        api:
            pattern:  ^/api
            http_basic: ~
            provider: bprs_applink

    access_control:
        # require BPRS_APPLINK for /api*
        - { path: ^/api, roles: ROLE_BPRS_APPLINK }
```
##Use Bundle
