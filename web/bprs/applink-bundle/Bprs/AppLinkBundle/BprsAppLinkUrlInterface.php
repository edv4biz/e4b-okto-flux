<?php

namespace Bprs\AppLinkBundle;

interface BprsAppLinkUrlInterface {
    public function getUrls();
}

?>
