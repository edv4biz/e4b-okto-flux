<?php

namespace Bprs\AssetBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Asset
 *
 * @ORM\MappedSuperclass
 * @ORM\HasLifecycleCallbacks()
 * @ORM\EntityListeners({"Bprs\AssetBundle\Event\LifecycleListener"})
 * @JMS\ExclusionPolicy("all")
 * @JMS\AccessType("public_method")
 */
class Asset
{
    /**
     * The unique name + extension of the asset (for example '557802a0f0801.png')
     * @var string
     * @JMS\Expose
     * @JMS\Type("string")
     * @ORM\Column(name="filekey", type="string", length=25)
     */
    private $filekey;

    /**
     * The IANA mimetype (for example 'image/png')
     * @var string
     * @JMS\Expose
     * @JMS\Type("string")
     * @ORM\Column(name="mimetype", type="string", length=50, nullable=true)
     */
    private $mimetype;

    /**
     * The adapter the file is stored in (see the bundle config or the readme)
     * @var string
     * @ORM\Column(name="adapter", type="string", length=50)
     * @JMS\Expose
     * @JMS\Type("string")
     */
    private $adapter;

    /**
     * Date of creation the entry in the database. Not of the asset!
     * @var \DateTime
     * @ORM\Column(name="created_at", type="datetime")
     * @JMS\Expose
     * @JMS\Type("DateTime")
     * @JMS\Accessor(getter="getCreatedAt",setter="setCreatedAt")
     */
    private $created_at;

    /**
     * Date of last change in the database. Not necessary of the asset file!
     * @var \DateTime
     * @ORM\Column(name="updated_at", type="datetime")
     * @JMS\Expose
     * @JMS\Type("DateTime")
     * @JMS\Accessor(getter="getUpdatedAt",setter="setUpdatedAt")
     */
    private $updated_at;

    /**
     * You can store the md5 hash of an asset here. however, due to long computing time,
     * this can't be done automatically. You can however, use something like the BPRSCommandlinkBundle to do this in the background
     * @var string
     * @ORM\Column(name="md5", type="string", length=32, nullable=true)
     * @JMS\Expose
     * @JMS\Type("string")
     */
    private $md5;

    /**
     * filesize of your asset in bytes. Can produce unexpected results on 32bit machines dough!
     * @ORM\Column(name="filesize", type="bigint", nullable=true)
     * @var integer
     */
    private $filesize;
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Expose
     * @JMS\ReadOnly
     */
    private $id;

    /**
    * @ORM\Column(name="name", type="string", length=1024)
    * @JMS\Expose
    * @JMS\Type("string")
    */
    private $name;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Asset
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    public function __toString() {
        return $this->filekey;
    }

    public function __construct()
    {
        $this->filekey = uniqID();
    }

    /**
     * Set key
     *
     * @param string $filekey
     * @return Asset
     */
    public function setFilekey($filekey)
    {
        $this->filekey = $filekey;

        return $this;
    }

    /**
     * Get key
     *
     * @return string
     */
    public function getFilekey($withoutextension = false)
    {
        if ($withoutextension) {
            return explode('.', $this->filekey)[0];
        }
        return $this->filekey;
    }

    /**
     * Set adapter
     *
     * @param string $adapter
     * @return Asset
     */
    public function setAdapter($adapter)
    {
        $this->adapter = $adapter;

        return $this;
    }

    /**
     * Get adapter
     *
     * @return string
     */
    public function getAdapter()
    {
        return $this->adapter;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAt()
    {
        $this->created_at = new \DateTime();
        return $this;
    }

    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function setUpdatedAt()
    {
        $this->updated_at = new \DateTime();
        return $this;
    }

    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set mimetype
     *
     * @param string $mimetype
     * @return Asset
     */
    public function setMimetype($mimetype)
    {
        $this->mimetype = $mimetype;

        return $this;
    }

    /**
     * Get mimetype
     *
     * @return string
     */
    public function getMimetype()
    {
        return $this->mimetype;
    }

    /**
     * Set md5
     *
     * @param string $md5
     * @return Asset
     */
    public function setMd5($md5)
    {
        $this->md5 = $md5;

        return $this;
    }

    /**
     * Get md5
     *
     * @return string
     */
    public function getMd5()
    {
        return $this->md5;
    }

    /**
     * Set filesize
     *
     * @param integer $filesize
     * @return Source
     */
    public function setFilesize($filesize)
    {
        $this->filesize = $filesize;

        return $this;
    }

    /**
     * Get filesize
     *
     * @return integer
     */
    public function getFilesize()
    {
        return $this->filesize;
    }
}
