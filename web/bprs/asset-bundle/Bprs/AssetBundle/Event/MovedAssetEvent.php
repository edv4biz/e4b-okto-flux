<?php

namespace Bprs\AssetBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class MovedAssetEvent extends Event
{
    protected $asset;

    public function __construct($asset) {
        $this->asset = $asset;
    }

    public function getAsset() {
        return $this->asset;
    }
}
