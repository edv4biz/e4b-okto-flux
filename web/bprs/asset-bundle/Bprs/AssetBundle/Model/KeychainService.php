<?php

namespace Bprs\AssetBundle\Model;

use GuzzleHttp\Client;
use Symfony\Componenet\HttpFoundation\Response;

class KeychainService {

    private $applink_service;
    private $jms_serializer;
    private $asset_class;

    public function __construct($applink_service, $serializer, $asset_class)
    {
        $this->applink_service = $applink_service;
        $this->jms_serializer = $serializer;
        $this->asset_class = $asset_class;
    }

    public function getAsset($keychain, $filekey)
    {
        $client = new Client(['verify' => false ]);
        try {
            $response = $client->request(
                'GET',
                $this->applink_service->getApiUrlsForKey($keychain, 'bprs_asset_api_metadata'),
                [
                    'auth' => [$keychain->getUser(), $keychain->getApiKey()],
                    'query' => ['filekey' => $filekey]
                ]
            );
            if ($response->getStatusCode() == 200) {
                $asset = $this->jms_serializer->deserialize(
                    $response->getBody(),
                    $this->asset_class,
                    'json'
                );
                return $asset;
            }
            return false;
        } catch (Exception $e) {
            return false;
        }
    }
}
