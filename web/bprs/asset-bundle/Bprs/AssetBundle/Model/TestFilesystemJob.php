<?php

namespace Bprs\AssetBundle\Model;

use Bprs\CommandLineBundle\Model\BprsContainerAwareJob as BaseJob;

class TestFilesystemJob extends BaseJob {

    /**
     * writes a testfile to given filesystem
     */
    public function perform() {
        $filesystem = $this->getContainer()->get('bprs.asset_helper')->getFilesystem($this->args['filesystem']);
        $filesystem->write('test_file.txt', 'test successfull!');
    }

    public function getName() {
        return 'Test Fileystem';
    }
}
?>
