<?php

namespace Bprs\AssetBundle\Model;

use Oneup\UploaderBundle\Event\PostPersistEvent;
use Oneup\UploaderBundle\Event\PreUploadEvent;
use Bprs\AssetBundle\Entity\Asset;

class AssetService
{
    const ROUTE_ASSET     = "bprs_asset_download";
    const ROUTE_API_ASSET = "bprs_asset_api_download";

    private $em;
    private $originalFile;
    private $class;
    private $assetHelper;

    public function __construct($entityManager, $class, $assetHelper) {
        $this->em = $entityManager;
        $this->class = $class;
        $this->assetHelper = $assetHelper;
    }

    /**
     * Links an asset in the database to a real File in your Filesystem
     */
    public function uploadedAsset(PostPersistEvent $event) {
        // create and set info
        $file = $event->getFile();
        $asset = $this->createAsset();
        $asset->setName($this->originalFile->getClientOriginalName());
        $asset->setFilekey(method_exists($file, 'getKey') ? $file->getKey() : $file->getPath());
        $asset->setAdapter($event->getType());
        $asset->setFilesize(0);
        $asset->setMimetype(method_exists($file, 'mimetype') ? $file->mimetype() : $file->getMimetype());

        // saving
        $this->assetHelper->dispatchPreCreateAssetEvent($asset, $event);
        $this->em->persist($asset);
        $this->em->flush();
        $this->assetHelper->dispatchPostCreateAssetEvent($asset, $event);

        // enhance response
        $response = $event->getResponse();
        $response['asset'] = $asset->getFilekey();
        $response['type'] = $asset->getMimetype();
        $response['url'] = $this->assetHelper->getAbsoluteUrl($asset);
        $response['filename'] = $asset->getName();
    }

    /**
     * Stores all information of the file before the upload starts
     */
    public function preUpload(PreUploadEvent $event) {
        $this->originalFile = $event->getFile();
    }

    /**
     * Removes the Asset from the database.
     *
     * @param  Asset  $asset [description]
     * @return [type]        [description]
     */
    public function deleteAsset(Asset $asset) {
        // $this->assetHelper->dispatchDeleteAssetEvent($asset);
        $this->em->remove($asset);
        $this->em->flush();
    }

    public function moveAsset($asset, $adapter)
    {
        if ($this->assetHelper->moveAsset($asset, $adapter)) {
            $asset->setAdapter($adapter);
            $this->em->persist($asset);
            $this->em->flush();
            $this->assetHelper->dispatchUpdatedAssetEvent($asset);
        }
    }

    public function getAsset($filekey) {
        return $this->em->getRepository($this->class)->findOneBy(['filekey' => $filekey]);
    }

    public function getRepository()
    {
        return $this->em->getRepository($this->class);
    }

    public function createAsset()
    {
        $asset = new $this->class;
        return $asset;
    }

    public function getHelper()
    {
        return $this->assetHelper;
    }
}
