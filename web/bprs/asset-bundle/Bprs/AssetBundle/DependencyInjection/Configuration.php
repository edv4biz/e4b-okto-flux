<?php

namespace Bprs\AssetBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder() {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('bprs_asset');
        $rootNode
            ->children()
                ->booleanNode('xsendfile')->defaultTrue()->end()
                ->scalarNode('worker_queue')->defaultValue('bprs_asset')->end()
                ->scalarNode('class')->isRequired()->end()
                ->scalarNode('filesystem_map')->isRequired()->end()
                ->scalarNode('404')->isRequired()->end()
                ->scalarNode('thumb_filesystem')->isRequired()->end()
                ->arrayNode('adapters')
                    ->requiresAtLeastOneElement()
                        ->children()
                            ->booleanNode('remote')->defaultFalse()->end()
                        ->end()
                        ->prototype('array')
                            ->prototype('scalar')->end()
                            ->isRequired()
                            ->useAttributeAskey('name')
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
