<?php

namespace Bprs\AssetBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\Table;

class CheckAssetsCommand extends ContainerAwareCommand {

    public function __construct() {
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('bprs:assetbundle:check_assets')
            ->setDescription('Checks if assets are really pointing to an existing file')
            ->addOption('adapter', null, InputOption::VALUE_REQUIRED, 'The Adapter you want to check')
            ->addOption('names_only', null, InputOption::VALUE_NONE, 'returns only the filenames')
            ->addOption('delete', null, InputOption::VALUE_NONE, 'try delete assets from the filesystem');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $asset_service = $this->getContainer()->get('bprs.asset');
        $assets_okay = [];
        $assets_not_okay = [];
        if ($input->getOption('adapter')) {
            $adapter = $input->getOption('adapter');
            $assets = $this->getContainer()->get('bprs.asset')->getRepository()->findBy(['adapter' => $adapter]);
            $filemanager = $this->getContainer()->get('bprs.asset_helper')->getFilesystem($adapter);

            foreach ($assets as $asset) {
                if ($filemanager->has($asset->getFilekey())) {
                    $assets_okay[] = $asset;
                } else {
                    $assets_not_okay[] = $asset;
                }
            }

        } else { //check all adapters
            $adapters = $this->getContainer()->getParameter('bprs_asset.adapters');
            foreach ($adapters as $adapter => $info) {
                $assets = $this->getContainer()->get('bprs.asset')->getRepository()->findBy(['adapter' => $adapter]);
                $filemanager = $this->getContainer()->get('bprs.asset_helper')->getFilesystem($adapter);

                foreach ($assets as $asset) {
                    if ($filemanager->has($asset->getFilekey())) {
                        $assets_okay[] = $asset;
                    } else {
                        $assets_not_okay[] = $asset;
                    }
                }
            }
        }

        $output->writeln('Following assets are not found in the filesystem');
        if ($input->getOption('names_only')) {
            foreach ($assets_not_okay as $asset) {
                $output->writeln($asset->getFilekey());
            }
        } else {
            $table = new Table($output);
            $rows = [];
            foreach ($assets_not_okay as $asset) {
                $rows[] = [$asset->getFilekey(), $asset->getAdapter()];
            }
            $table
                ->setHeaders(['filekey', 'adapter'])
                ->setRows($rows);

            $table->render();
        }

        if ($input->getOption('delete')) {
            foreach ($assets_not_okay as $asset) {
                $asset_service->deleteAsset($asset);
            }
        }
    }

}
