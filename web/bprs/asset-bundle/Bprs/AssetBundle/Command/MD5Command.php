<?php

namespace Bprs\AssetBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class MD5Command extends ContainerAwareCommand {

    public function __construct() {
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('bprs:assetbundle:calc_md5')
            ->setDescription('Generates missing MD5 without a job');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $assets = $this->getContainer()->get('bprs_asset.repository')->findBy(['md5' => null]);
        $helper = $this->getContainer()->get('bprs.asset_helper');
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $output->writeln(sprintf("Found %s Assets without Checksum", count($assets)));
        $output->writeln("Starting Calculations");
        foreach ($assets as $asset) {
            $asset->setMd5(md5_file($helper->getAbsoluteUrl($asset)));
        }
        $output->writeln("Starting Batch Update");
        $batch = 0;
        foreach ($assets as $asset) {
            $em->persist($asset);
            $batch++;
            if ($batch > 10) {
                $em->flush();
                $em->clear();
                $batch = 0;
            }
        }
        $em->flush();
        $em->clear();

        $output->writeln("Updated all missing MD5 checksums.");

        $assets = $this->getContainer()->get('bprs_asset.repository')->findBy(['md5' => null]);
        if ($assets) {
            $output->writeln("These Assets are still missing a checksum");
            foreach ($assets as $asset) {
                $output->writeln($asset->getFilekey());
            }
        }
    }
}
