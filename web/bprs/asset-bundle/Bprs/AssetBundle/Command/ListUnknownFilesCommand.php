<?php

namespace Bprs\AssetBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\Table;

class ListUnknownFilesCommand extends ContainerAwareCommand {

    public function __construct() {
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('bprs:assetbundle:check_for_unknown_files')
            ->setDescription('Returns a list of Files where no asset exists in the database')
            ->addOption('adapter', null, InputOption::VALUE_REQUIRED, 'The Adapter you want to check')
            ->addOption('names_only', null, InputOption::VALUE_NONE, 'returns only the filenames')
            ->addOption('delete', null, InputOption::VALUE_NONE, 'try delete unknown files');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $asset_service = $this->getContainer()->get('bprs.asset');
        $unknowns = [];
        $knowns = [];
        if ($input->getOption('adapter')) {
            $adapter = $input->getOption('adapter');
            $filemanager = $this->getContainer()->get('bprs.asset_helper')->getFilesystem($adapter);
            $files = $filemanager->listContents();

            foreach ($files as $file) {
                $asset = $asset_service->getAsset($file);
                if ($asset) {
                    $knowns[] = $asset;
                } else {
                    $unknowns[] = $file;
                    if ($input->getOption('delete')) {
                        $filemanager->delete($file['basename']);
                    }
                }
            }
        } else { //check all adapters
            $adapters = $this->getContainer()->getParameter('bprs_asset.adapters');
            foreach ($adapters as $adapter => $info) {
                $filemanager = $this->getContainer()->get('bprs.asset_helper')->getFilesystem($adapter);

                $files = $filemanager->listContents();
                foreach ($files as $file) {
                    if ($file["type"] == "file") {
                            $asset = $asset_service->getAsset($file);
                            if ($asset) {
                                $knowns[] = $asset;
                            } else {
                                $unknowns[] = $file;
                            }
                    }
                }
            }
        }

        if ($input->getOption('names_only')) {
            foreach ($unknowns as $unknown) {
                $output->writeln($unknown['basename']);
            }
        } else {
            $table = new Table($output);
            $rows = [];
            foreach ($unknowns as $unknown) {
                $rows[] = [$unknown['basename'], $unknown['path']];
            }
            $table
                ->setHeaders(['filekey', 'path'])
                ->setRows($rows);

            $table->render();
        }
    }

}
