<?php

namespace Bprs\AnalyticsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Security\Core\Encoder\BCryptPasswordEncoder;
use GeoIp2\Exception\AddressNotFoundException;
use GeoIp2\Exception\InvalidArgumentException;

class ExportAnalyticsCommand extends ContainerAwareCommand {

    public function __construct()
    {
        parent::__construct();
    }

    protected function configure() {
        $this
            ->setName('bprs:analytics:export')
            ->addArgument('email', InputArgument::REQUIRED, 'The recipient for your analytics')
            ->addArgument('identifier', InputArgument::REQUIRED, 'The Identifier for your logstates you want to export')
            ->addOption('start', null, InputOption::VALUE_REQUIRED, 'DateTime String for the earliest logstates', '-1 day')
            ->addOption('end', null, InputOption::VALUE_REQUIRED, 'DateTime String for the newest logstates', 'now')
            ->setDescription('Exports Analytics as CSV in given Timeframe');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $analytics_service = $this->getContainer()->get('bprs_analytics');
        $logstates = $analytics_service->getLogstatesInTime(
            ['identifier' => $input->getArgument('identifier')],
            $input->getOption('start'),
            $input->getOption('end')
        );

        $mailer = $this->getContainer()->get('bprs_user.mailer');
        $message = $mailer->createMail($input->getArgument('email'), 'BprsAnalytcs Export');

        $file = tmpfile();
        $delimiter = ';';
        fputcsv(
            $file,
            [
                'referer',
                'url',
                'identifier',
                'value',
                'country',
                'city',
                'timestamp',
                'user-agent',
                'platform',
                'browser',
                'parent',
                'version',
                'majorver',
                'minorver',
                'cssversion',
                'mobile',
                'bot'
            ],
            $delimiter
        );

        foreach ($logstates as $logstate) {
            fputcsv(
                $file,
                [
                    $logstate->getReferer(),
                    $logstate->getUrl(),
                    $logstate->getIdentifier(),
                    $logstate->getValue(),
                    $logstate->getCountry(),
                    $logstate->getCity(),
                    $logstate->getTimestamp()->format('H:i d.m.Y'),
                    $logstate->getUserAgent(),
                    $logstate->getPlatform(),
                    $logstate->getBrowser(),
                    $logstate->getParent(),
                    $logstate->getVersion(),
                    $logstate->getMajorver(),
                    $logstate->getMinorver(),
                    $logstate->getCssversion(),
                    $logstate->getMobile() ? '1' : '0',
                    $logstate->getBot() ? '1' : '0'
                ],
                $delimiter
            );
        }
        rewind($file);

        $attachment = new \Swift_Attachment(stream_get_contents($file), 'logstates.csv', 'text/csv');
        $message->attach($attachment);

        $mailer->send($message);
    }
}
