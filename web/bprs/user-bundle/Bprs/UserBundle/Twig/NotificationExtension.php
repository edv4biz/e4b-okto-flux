<?php

namespace Bprs\UserBundle\Twig;

class NotificationExtension extends \Twig_Extension
{
    private $repo;

    public function __construct($repo)
    {
        $this->repo = $repo;
    }

    public function getFunctions() {
        return array(
            new \Twig_SimpleFunction('newestNotifications', array($this, 'newestNotifications'))
        );
    }

    public function newestNotifications($user)
    {
        $notifications = $this->repo->findNewestNotificationsForUser($user);
        return $notifications;
    }

    public function getName() {
        return 'oktothek_notification_extension';
    }
}
