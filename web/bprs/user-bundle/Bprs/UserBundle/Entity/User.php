<?php

namespace Bprs\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
//use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * User
 * @UniqueEntity(
 *     fields = "email",
 *     errorPath = "email",
 *     message = "bprs_user.email.already_in_use"
 *     )
 * @UniqueEntity(
 *     fields = "username",
 *     errorPath = "username",
 *     message = "bprs_user.username.already_in_use"
 *     )
 * @ORM\MappedSuperclass
 * @ORM\HasLifecycleCallbacks()
 */
 class User implements /*AdvancedUserInterface*/ UserInterface, \Serializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Assert\NotBlank(message = "bprs_user.user.username_notblank", groups={"bprs_user", "bprs_user_username"})
     * @ORM\Column(name="username", type="string", length=255, unique=true)
     */
    private $username;

    /**
     * @Assert\NotBlank(message = "bprs_user.user.password_notblank", groups={"iforgot"} )
     * @Assert\Length(
     *      min = 6,
     *      minMessage = "bprs_user.user.password_lengthMin"
     * )
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=true)
     */
    private $password;

    /**
     * @Assert\NotBlank(message = "bprs_user.user.email_notblank", groups={"bprs_user", "bprs_user_email"} )
     * @Assert\Email(
     *     message = "bprs_user.user.email_wrong",
     *     checkMX = true,
     *     groups={"bprs_user", "bprs_user_email"}
     * )
     *
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     */
    private $email;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isActive", type="boolean")
     */
    private $isActive;

    /**
     * @ORM\ManyToMany(targetEntity="dbRole")
     * @ORM\JoinTable(name="user_role",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")}
     *      )
     */
    private $roles;

    /**
     * @ORM\Column(name="resetHash", type="string", length=40, nullable=true)
     */
    private $resetHash;

    /**
     * @ORM\OneToMany(targetEntity="Bprs\UserBundle\Entity\Notification", mappedBy="user")
     */
    protected $notifications;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    public function __toString():string
    {
        return $this->username;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    public function __construct()
    {
        $this->isActive = true;
        $this->resetHash = null;
        $this->roles = new ArrayCollection();
        $this->notifications = new ArrayCollection();
    }

    /**
     * @inheritDoc
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function getRoles()
    {
        return $this->roles->toArray();
    }

    public function getRoleObjects()
    {
        return $this->roles;
    }

    public function addRole($role)
    {
        $this->roles[] = $role;
        return $this;
    }

    public function removeRole($role)
    {
        $this->roles->removeElement($role);
    }

    /**
     * @inheritDoc
     */
    public function eraseCredentials()
    {
    }

    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return true;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    public function isEnabled()
    {
        return $this->isActive;
    }

    public function setResetHash($resetHash)
    {
        $this->resetHash = $resetHash;
    }

    public function getResetHash()
    {
        return $this->resetHash;
    }

    /**
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            $this->isActive
        ));
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            $this->isActive
        ) = unserialize($serialized);
    }

    public function getNotifications()
    {
        return $this->notifications;
    }

    public function addNotification($notification)
    {
        $this->notifications[] = $notification;
        $notification->setUser($this);
        return $this;
    }

    public function removeNotification($notification)
    {
        $this->notifications->removeElement($notification);
        return $this;
    }

    public function setNotifications($notifications)
    {
        $this->notifications = $notifications;
        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAt()
    {
        $this->created_at = new \DateTime();
        return $this;
    }

    public function getCreatedAt()
    {
        return $this->created_at;
    }
}
