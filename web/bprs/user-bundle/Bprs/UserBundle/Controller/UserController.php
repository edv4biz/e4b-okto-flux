<?php

namespace Bprs\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Bprs\UserBundle\Form\UserType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

/**
 * @Route("/bprs_user_/backend/admin/user")
 * @Security("is_granted('ROLE_BPRS_USER_BACKEND')")
 */
class UserController extends Controller
{
    /**
     * @Route("/{page}", name="bprs_user_backend_users", defaults={"page" = 1}, requirements={"page": "\d+"})
     * @Template()
     */
    public function list_usersAction($page = 1)
    {
        $em = $this->getDoctrine()->getManager();
        $userclass = $this->container->getParameter('bprs_user.class');
        $query = $em->getRepository($userclass)->getUsers($userclass, true);
        $paginator = $this->get('knp_paginator');
        $users = $paginator->paginate($query, $page, 10);
        return array('users' => $users);
    }

    /**
     * @Route("/new", name="bprs_user_backend_user_new")
     * @Template()
     */
    public function new_userAction(Request $request)
    {
        $userclass = $this->container->getParameter('bprs_user.class');
        $user = new $userclass;
        $form = $this->createForm(UserType::class, $user);
        $form->add('save', SubmitType::class, ['attr' => ['class' => 'btn btn-primary'], 'label' => 'bprs_user.new_user.submit']);

        if ($request->getMethod() == "POST") { //form send
            $form->handleRequest($request);

            if ($form->isValid()) {
                $this->get('bprs_user.user')->createUser($user);
                $this->get('session')->getFlashBag()->add('success', 'bprs_user.message.user_create_success');
                return $this->redirect($this->generateUrl('bprs_user_backend_users'));
            }
            $this->get('session')->getFlashBag()->add('error', 'bprs_user.message.user_create_error');
        }
        return array('form' => $form->createView());
    }

    /**
     * @Route("/{id}/edit", name="bprs_user_backend_user_edit")
     * @Template()
     */
    public function edit_userAction(Request $request, $id)
    {
        $user = $this->get('bprs_user.user')->getUserBy(['id' => $id]);
        $form = $this->createForm(UserType::class, $user, ['validation_groups' => ['bprs_user']]);
        $form->add(
            'save',
            SubmitType::class,
            [
                'attr' => ['class' => 'btn btn-primary'],
                'label' => 'bprs_user.edit_user.submit']
            );
        $form->add(
            'delete',
            SubmitType::class,
            [
                'attr' => ['class' => 'btn btn-link'],
                'label' => 'bprs_user_edit_user_delete']
            );
        $em = $this->getDoctrine()->getManager();

        if ($request->getMethod() == "POST") { //form sent
            $form->handleRequest($request);
            if ($form->isValid()) { // update
                if ($form->get('save')->isClicked()) {
                    $em->persist($user);
                    $em->flush();
                    $this->get('session')->getFlashBag()->add(
                        'success',
                        "bprs_user.message.user_edit_success"
                    );
                    return $this->redirect($this->generateUrl('bprs_user_backend_users'));
                }  elseif ($form->get('delete')->isClicked()) { // delete
                    $this->get('bprs_user.user')->deleteUser($user);
                    $this->get('session')->getFlashBag()->add(
                        'success',
                        'bprs_user.message.user_delete_success'
                    );
                    return $this->redirect($this->generateUrl('bprs_user_backend_users'));
                }

            }
            $this->get('session')->getFlashBag()->add('error', "bprs_user.message.user_edit_error");
        }

        $all_roles = $em->getRepository('BprsUserBundle:Role')->findAll();
        $open_roles = array_diff($all_roles, $user->getRoles());

        return ['form' => $form->createView(), 'roles' => $open_roles];
    }
}
