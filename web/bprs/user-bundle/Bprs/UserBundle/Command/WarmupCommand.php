<?php

namespace Bprs\UserBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Bprs\UserBundle\Entity\dbRole;

class WarmupCommand extends Command
{
    private $em;
    private $roles;

    public function __construct($entityManager, $user_roles)
    {
        $this->em = $entityManager;
        $this->roles = $user_roles;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('bprs:userbundle:warmup')
            ->setDescription('Installs Roles. Run this after installing or update');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        foreach ($this->roles as $role) {
            $check = $this->em->getRepository('BprsUserBundle:Role')->findOneBy(array('name' => $role));
            if (!$check) {
                $output->writeln(sprintf('Adding Role [%s]', $role));
                $permission = new dbRole();
                $permission->setName($role);
                $this->em->persist($permission);
            }
        }
        $this->em->flush();

        $output->writeln("Warmup Complete! You may want to add an ADMIN user to the Database with [bprs:userbundle:create_admin] next");
    }
}
