<?php

namespace Bprs\UserBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('bprs_user');

        $rootNode
            ->children()
                ->scalarNode('class')->isRequired()->end()
                ->scalarNode('notification_class')->defaultValue("BprsUserBundle:Notification")->end()
                ->scalarNode('register_header')->defaultValue('Registered')->end()
                ->scalarNode('iforgot_header')->defaultValue('iForgot')->end()
                ->arrayNode('mail')
                    ->children()
                        ->scalarNode('commandline_host')->end()
                        ->scalarNode('adress')->end()
                        ->scalarNode('name')->end()
                    ->end()
                ->end()
                ->arrayNode('permissions')
                    ->prototype('scalar')->end()
                ->end()
                ->arrayNode('user_defaults')
                    ->prototype('scalar')->end()
                ->end()
            ->end()
        ;

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.

        return $treeBuilder;
    }

    public function getAlias()
    {
        return 'bprs_user';
    }
}
