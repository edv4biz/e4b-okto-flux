<?php

namespace Bprs\UserBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class BprsUserExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');

        $container->setParameter('bprs_user.mail.commandline_host', $config['mail']['commandline_host']);
        $container->setParameter('bprs_user.mail.adress', $config['mail']['adress']);
        $container->setParameter('bprs_user.mail.name', $config['mail']['name']);
        $container->setParameter('bprs_user.class', $config['class']);
        $container->setParameter('bprs_user.notification_class', $config['notification_class']);
        $container->setParameter('bprs_user.permissions', $config['permissions']);
        $container->setParameter('bprs_user.user_defaults', $config['user_defaults']);
        $container->setParameter('bprs_user.mail.register_header', $config['register_header']);
        $container->setParameter('bprs_user.mail.iforgot_header', $config['iforgot_header']);
    }
}
