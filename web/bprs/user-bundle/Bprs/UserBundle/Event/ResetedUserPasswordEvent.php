<?php

namespace Bprs\UserBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class ResetedUserPasswordEvent extends Event {

    private $user;

    public function __construct($user)
    {
        $this->user = $user;
    }

    public function getUser()
    {
        return $this->user;
    }
}
