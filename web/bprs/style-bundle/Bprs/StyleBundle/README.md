# BPRSStyleBundle
Simple Bootstrap Styling for your symfony applications.

It contains form theming, js, css and flashbags with translation activated

## install
### get bundle
on the commandline in your projectfolder, use composer  
`composer require: bprs/style-bundle`

### activate bundle
AppKernel  
`new Bprs\StyleBundle\BprsStyleBundle()`

## usage
The basic idea is a simple reusable layout.  
in your template, use    
```twig
{% extends "BprsStyleBundle::layout.html.twig" %}

{% block nav_head%}
    {% include 'BprsStyleBundle::menu.html.twig' with {'selected': '#selected_menu_item#', 'dropdown': '#selected_dropdown_item#'} %}
{% endblock %}
{% block body_main %}
    {#your page content#}
{% endblock %}
```
in your config.yml, add:
```yml
twig
    globals:
        version: "YourVersion"
        name: "YourProjectName"
```
### use form theming
in your config.yml, add:
```yml
form:
    resources:
        - 'BprsStyleBundle:Form:fields.html.twig'
```

### use character counter
include the js file in your view 
```twig
<script src="{{ asset('bundles/bprsstyle/js/charactercounter.js')}}"></script>
```

active the counter on your jquery object like
```js
$("#my_textarea").characterCounter({
            counterCssClass: 'help-block',
            limit: 1000
            });
```

## overwrite bundle
add `app/Resources/BprsStylBundle/views/menu.html.twig`  
add `app/Resources/BprsStylBundle/views/layout.html.twig`  
and edit it accordingly.
'selected' allows highlighting of a main menu point, dropdown allows highlighting of a menu entry.
