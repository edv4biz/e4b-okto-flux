<?php

namespace Bprs\LogbookBundle\Model;

use Bprs\LogbookBundle\Entity\Entry;
use Bprs\LogbookBundle\Entity\Value;

class LogbookService {

    private $em;
    private $translator;

    public function __construct($em, $trans)
    {
        $this->em = $em;
        $this->translator = $trans;
    }

    /**
     * @param the transkey you want to use for your
     * @param integer $type the type of messag you want to log. Check Entry.php consts for available types
     * @param string $reference the uniqID or other reference you want to use to find loglines later
     * @param array $values ["name" => "max mustermann"] you want to log for that information (used for translations and more)
     */
    public function write($transkey, $type = Entry::LOGTYPE_INFO, $reference = null, $values = [])
    {
        $entry = new Entry();
        $entry->setLine($this->translator->trans($transkey, $values));
        $entry->setTranskey($transkey);
        $entry->setType($type);
        $entry->setReference($reference);


        foreach ($values as $transkey => $transvalue) {
            $value = new Value();
            $value->setValueKey($transkey);
            $value->setValue($transvalue);
            $entry->addValue($value);
            $this->em->persist($value);
        }

        $this->em->persist($entry);
        $this->em->flush();
    }

    public function info($transkey, $values =[], $reference = null)
    {
        $this->write($transkey, Entry::LOGTYPE_INFO, $reference, $values);
    }

    public function warning($transkey, $values =[], $reference = null)
    {
        $this->write($transkey, Entry::LOGTYPE_WARNING, $reference, $values);
    }

    public function error($transkey, $values =[], $reference = null)
    {
        $this->write($transkey, Entry::LOGTYPE_ERROR, $reference, $values);
    }
}

 ?>
