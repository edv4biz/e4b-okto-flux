<?php

namespace Bprs\LogbookBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Entry
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Bprs\LogbookBundle\Entity\EntryRepository")
 */
class Entry
{
    const LOGTYPE_INFO = 0;
    const LOGTYPE_WARNING = 10;
    const LOGTYPE_ERROR = 20;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="line", type="string", length=300)
     */
    private $line;

    /**
    * @var string
    *
    * @ORM\Column(name="transkey", type="string", length=50)
    */
    private $transkey;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=23, nullable=true)
     */
    private $reference;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="smallint")
     */
    private $type;

    /**
     * @var array
     *
     * @ORM\OneToMany(targetEntity="Value", mappedBy="entry", cascade={"remove", "persist"}, fetch="EAGER")
     */
    private $values;

    /**
     * @ORM\Column(name="createdAt", type="datetime")
     */
    private $createdAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function __construct()
    {
        $this->createdAt = new \Datetime();
        $this->values = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->line;
    }

    /**
     * Set line
     *
     * @param string $line
     * @return Entry
     */
    public function setLine($line)
    {
        $this->line = $line;

        return $this;
    }

    /**
     * Get line
     *
     * @return string
     */
    public function getLine()
    {
        return $this->line;
    }

    /**
     * Set reference
     *
     * @param string $reference
     * @return Entry
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    public function transValues()
    {
        $val = [];
        foreach ($this->getValues() as $value) {
            $val[$value->getValueKey()] = $value->getValue();
        }
        return $val;
    }

    /**
     * Get reference
     *
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set transkey
     *
     * @param string $transkey
     * @return Entry
     */
    public function setTranskey($transkey)
    {
        $this->transkey = $transkey;

        return $this;
    }

    /**
     * Get transkey
     *
     * @return string
     */
    public function getTranskey()
    {
        return $this->transkey;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return Entry
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Entry
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Add values
     *
     * @param \Bprs\LogbookBundle\Entity\Value $values
     * @return Entry
     */
    public function addValue(\Bprs\LogbookBundle\Entity\Value $values)
    {
        $this->values[] = $values;
        $values->setEntry($this);

        return $this;
    }

    /**
     * Remove values
     *
     * @param \Bprs\LogbookBundle\Entity\Value $values
     */
    public function removeValue(\Bprs\LogbookBundle\Entity\Value $values)
    {
        $this->values->removeElement($values);
    }

    /**
     * Get values
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getValues()
    {
        return $this->values;
    }
}
