# BprsLogbookBundle
Transation based logging for your needs

## Info
### What does it do?
Writes a logline as translationkey, values, timestamp, reference and a default translated info to the database

## Installation
### Terminal
```
composer require bprs/logbook-bundle
```

### AppKernel.php
```php
new Bprs\LogbookBundle\BprsLogbookBundle(),
```

### Routing
bprs_logbook:
    resource: "@BprsLogbookBundle/Controller/"
    type:     annotation
    prefix:   /{_locale}

## Usage Writing
### In a controller
There are several ways to write a log line

```php
$logbook = $this->get('bprs_logbook');
$reference = $YOUR_UNIQUE_REFERENCE; // id, hash, whatever in 23 characters to reference to
//quick info:
$logbook->info('your_trans_key', ["%your_values%" => 'for_the_tranlation'], $reference);

//quick warning;
$logbook->warning('your_trans_key', ["%your_values%" => 'for_the_tranlation'], $reference);

//quick error;
$logbook->error('your_trans_key', ["%your_values%" => 'for_the_tranlation'], $reference);

//more control

use Bprs\LogbookBundle\Entity\Entry;

$logbook->write('your_trans_key', Entry::LOGTYPE_INFO, $reference, ['%your_values%' => 'for_the_translation']);
```

### At the commandline
```
./app/console OR ./bin/console

bprs:logbook:write -r REFERENCE -t TYPE your_values:for_the_translation

# Example
./bin/console bprs:logbook:write "oktolab_media.edit_series_header" -r A3F322 "%series_name%:MY Series"

```

## Usage Showing
### In a twig template

The Bundle uses the BprsStyleBundle (Bootstrap) for styling.

```twig
<h2>Logs</h2>
{% render(controller('BprsLogbookBundle:Default:pager', {'reference': entity.uniqID}))%}

```
Where the reference is your reference to show a log history

### At the commandline
```
./app/console OR ./bin/console

bprs:logbook:read -r REFERENCE -f php_datetime_string_from -t php_datetime_string_to

# Example
./bin/console bprs:logbook:read -r A3F322

```
You'll get a table with all Loglines in the last 2 days for this reference. 
