<?php

namespace Bprs\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Bprs\UserBundle\Entity\Notification;

/**
 * @Route("/bprs_user_/notification")
 */
class NotificationController extends Controller
{
    /**
     * @Route("s", name="bprs_user_notifications")
     * @Template()
     */
    public function index(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository('BprsUserBundle:Notification')->findNotificationsForUser($this->getUser(), true);
        $notifications = $this->get('knp_paginator')->paginate(
            $query,
            $request->query->get('page', 1),
            $request->query->get('results', 10)
        );

        return ['notifications' => $notifications];
    }

    /**
     * @Route("/follow/{notification}", name="bprs_user_notification_follow")
     */
    public function show(Notification $notification)
    {
        $this->denyAccessUnlessGranted('user_follow', $notification);
        $this->get('bprs_user.notification')->markAsRead($notification);
        return $this->redirect($notification->getLink());
    }
}
