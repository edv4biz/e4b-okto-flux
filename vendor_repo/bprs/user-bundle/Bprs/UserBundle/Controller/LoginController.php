<?php

namespace Bprs\UserBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Bprs\UserBundle\Entity\dbRole;
use Bprs\UserBundle\Form\RegisterType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
* @Route("/login")
*/
class LoginController extends Controller
{
    /**
     * @Route("/", name="bprs_user_login")
     * @Template()
     */
    public function loginAction(Request $request)
    {
        $authenticationUtils = $this->get('security.authentication_utils');

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        if ($error) {
            $this->get('session')->getFlashBag()->add('error', $error->getMessage());
        }

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return array('last_username' => $lastUsername);
    }

    /**
     * @Route("/backend/login_check", name="bprs_user_login_check")
     */
    public function login_checkAction()
    {
         return $this->redirect($this->generateUrl('bprs_user_login'));
    }

    /**
     * @Route("/iforgot", name="bprs_user_iforgot")
     * @Template
     */
    public function reset_password_mailAction(Request $request)
    {
         $form = $this->createFormBuilder()
            ->add(
                'username',
                TextType::class,
                [
                    'label' => 'bprs_user.iforgot.username',
                    'constraints' => array(
                        new NotBlank(),
                        new Length(array('max' => 255))
                    )
                ]
            )
            ->add('save', SubmitType::class, array('label' => 'bprs_user.iforgot.reset_password_submit'))
            ->getForm();

        if ($request->getMethod() == "POST") { //form sent
            $form->handleRequest($request);
            if ($form->isValid()) {
                $data = $form->getData();
                $this->get('bprs_user.iforgot')->resetPassword($data['username']);

                $this->get('session')->getFlashBag()->add('info', 'bprs_user.message.iforgot_success');
                return $this->redirect($this->generateUrl('bprs_user_login'));
            }
            $this->get('session')->getFlashBag()->add('error', 'bprs_user.message.iforgot_error');
        }
        return array('form' => $form->createView());
    }

    /**
     * @Route("/iforgot/{resetHash}/new_password", name="bprs_user_iforgot_reset")
     * @Template
     */
    public function reset_passwordAction(Request $request, $resetHash)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('bprs_user.repository')->findOneBy(array('resetHash' => $resetHash));
        if (!$user) {
            return $this->redirect($this->generateUrl('bprs_user_login'));
        }

        $form = $this->createFormBuilder()
            ->add(
                'password',
                PasswordType::class,
                [
                    'label' => 'bprs_user.iforgot.password',
                    'constraints' => [
                        new NotBlank(),
                        new Length(['min' => 6])
                    ]
                ]
            )
            ->add('save', SubmitType::class, ['label' => 'bprs_user.iforgot.save_password'])
            ->getForm();

        if ($request->getMethod() == "POST") { //form sent
            $form->handleRequest($request);
            if ($form->isValid()) {
                $data = $form->getData();
                $this->get('bprs_user.iforgot')->setPassword($data['password'], $resetHash);

                $this->get('session')->getFlashBag()->add('success', 'bprs_user.message.iforgot_reset_success');
                return $this->redirect($this->generateUrl('bprs_user_login'));
            }
            $this->get('session')->getFlashBag()->add('error', 'bprs_user.message.iforgot_reset_error');
        }
        return array('form' => $form->createView());
    }

    /**
     * @Route("/register", name="bprs_user_register_user")
     * @Template()
     */
    public function registerUserAction(Request $request)
    {
        $class = $this->container->getParameter('bprs_user.class');
        $user = new $class;
        $form = $this->createForm(RegisterType::class, $user, [
            'action' => $this->generateUrl('bprs_user_register_user'),
            'method' => 'POST',
        ]);

        $form->add('submit', 'submit', ['label' => 'bprs_user.create_button']);

        if ($request->getMethod() == "POST") {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $default_roles = $this->getParameter('bprs_user.user_defaults');
                $this->get('bprs_user.user')->createUser($user, $default_roles);
                $this->get('session')->getFlashBag()->add('success', 'bprs_user.message.user_register_success');
                return $this->redirect($this->generateUrl('bprs_user_login'));
            }
            $this->get('session')->getFlashBag()->add('error', 'bprs_user.message.user_register_error');
        }
        return array('form' => $form->createView());
    }
}
