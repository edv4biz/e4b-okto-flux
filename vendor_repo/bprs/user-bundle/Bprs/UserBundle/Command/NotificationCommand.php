<?php

namespace Bprs\UserBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;

class NotificationCommand extends Command
{
    private $userRepo;
    private $notificationService;

    public function __construct($userRepo, $notificationService)
    {
        $this->userRepo = $userRepo;
        $this->notificationService = $notificationService;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('bprs:userbundle:notification')
            ->setDescription('Sends a notification to a given user')
            ->addArgument('user', InputOption::VALUE_REQUIRED, 'username/email')
            ->addArgument('url', InputOption::VALUE_REQUIRED, 'url the notifcation points to')
            ->addArgument('message', InputOption::VALUE_REQUIRED, 'your (translatable) message to display')
            ->addArgument('args', InputArgument::IS_ARRAY, 'all notification message related args (name:bprs input:a output:b)');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $user = $this->userRepo->loadUserByUsername($input->getArgument('user'));
        $args = $input->getArgument("args");
        $arguments = [];
        foreach ($args as $arg) {
            $parts = explode(':', $arg);
            $arguments[$parts[0]] = $parts[1];
        }
        $this->notificationService->addNewNotification($user, $input->getArgument('url'), $input->getArgument('message'), $arguments);
        $output->writeln("Sent out notification!");
    }
}
