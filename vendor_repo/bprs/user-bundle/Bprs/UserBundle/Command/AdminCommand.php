<?php

namespace Bprs\UserBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class AdminCommand extends ContainerAwareCommand
{
    private $userService;
    private $mailerHost;
    private $user;

    public function __construct($userService, $mailerHost, $user)
    {
        $this->userService = $userService;
        $this->mailerHost = $mailerHost;
        $this->user = new $user;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('bprs:userbundle:create_admin')
            ->setDescription('creates an admin user')
            ->addOption('name', '-1', InputOption::VALUE_REQUIRED, 'name of the account')
            ->addOption('email', '-2', InputOption::VALUE_REQUIRED, 'email of the account. Needed for password creation');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $context = $this->getContainer()->get('router')->getContext();
        $context->setHost($this->mailerHost);
        $this->user->setUsername($input->getOption('name'));
        $this->user->setEmail($input->getOption('email'));
        $all_permissions = $this->getContainer()->getParameter('bprs_user.permissions');
        $this->userService->createUser($this->user, $all_permissions);

        $output->writeln(sprintf('Created User %s', $this->user->getUsername()));
    }
}
