<?php

namespace Bprs\UserBundle;

final class BprsUserEvent
{
    /**
     * The bprs_user.post_user_create event is thrown each time after an user is created
     * in the system.
     *
     * The event listener receives an
     * Bprs\UserBundle\Event\CreatedUserEvent instance. Includes the user.
     *
     * @var string
     */
    const POST_USER_CREATE = 'bprs_user.post_user_create';

    /**
     * The bprs_user.post_password_reset event is thrown each time after an user has reset his password
     *
     * The event listener receives an
     * Bprs\UserBundle\Event\ResetedUserPasswordEvent instance. Included the user.
     */
    const POST_PASSWORD_RESET = 'bprs_user.post_password_reset';

    /**
     * The bprs_user.pre_user_delete event is thrown each time before an user is deleted
     * in the system.
     *
     * The event listener receives an
     * Bprs\UserBundle\Event\PreDeleteUserEvent instance. Includes the user.
     *
     * @var string
     */
    const PRE_USER_DELETE = 'bprs_user.pre_user_delete';
}
