<?php

namespace Bprs\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Table()
 * @ORM\Entity()
 */
class NotificationValue {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Exclude
     * @JMS\ReadOnly
     */
    private $id;

    /**
     * @ORM\Column(name="notificationkey", type="text", length=50)
     */
    protected $notificationkey;

    /**
     * @ORM\Column(name="notificationvalue", type="text", length=255)
     */
    protected $notificationvalue;

    /**
     * @ORM\ManyToOne(targetEntity="Bprs\UserBundle\Entity\Notification", inversedBy="values")
     */
    protected $notification;

    public function __construct($key = '', $value = '')
    {
        $this->notificationkey = $key;
        $this->notificationvalue = $value;
    }

    /**
     * Set notificationkey
     *
     * @param string $notificationkey
     * @return NotificationValue
     */
    public function setNotificationkey($notificationkey)
    {
        $this->notificationkey = $notificationkey;

        return $this;
    }

    /**
     * Get notificationkey
     *
     * @return string
     */
    public function getNotificationkey()
    {
        return $this->notificationkey;
    }

    /**
     * Set notificationvalue
     *
     * @param string $notificationvalue
     * @return NotificationValue
     */
    public function setNotificationvalue($notificationvalue)
    {
        $this->notificationvalue = $notificationvalue;

        return $this;
    }

    /**
     * Get notificationvalue
     *
     * @return string
     */
    public function getNotificationvalue()
    {
        return $this->notificationvalue;
    }

    /**
     * Set notification
     *
     * @param \Bprs\UserBundle\Entity\Notification $notification
     * @return NotificationValue
     */
    public function setNotification(\Bprs\UserBundle\Entity\Notification $notification = null)
    {
        $this->notification = $notification;

        return $this;
    }

    /**
     * Get notification
     *
     * @return \Bprs\UserBundle\Entity\Notification
     */
    public function getNotification()
    {
        return $this->notification;
    }
}
