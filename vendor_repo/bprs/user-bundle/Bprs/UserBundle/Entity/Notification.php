<?php

namespace Bprs\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Bprs\UserBundle\Entity\NotificationValue;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Notification
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Bprs\UserBundle\Entity\Repository\NotificationRepository")
 * @ORM\HasLifecycleCallbacks()
 * @JMS\ExclusionPolicy("all")
 * @JMS\AccessType("public_method")
 */
class Notification {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Exclude
     * @JMS\ReadOnly
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Bprs\UserBundle\Entity\UserInterface", inversedBy="notifications")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ORM\Column(name="message", type="string", length=255)
     */
    protected $message;

    /**
     * @ORM\OneToMany(targetEntity="Bprs\UserBundle\Entity\NotificationValue", mappedBy="notification", cascade={"persist", "remove"})
     */
    protected $values;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isread", type="boolean", options={"default" = false})
     */
    protected $read;

    /**
     * @var \DateTime
     * @JMS\Expose
     * @JMS\Type("DateTime")
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    public function __construct($message = "")
    {
        $this->message = $message;
        $this->createdAt = new \DateTime();
        $this->read = false;
        $this->values = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return Notification
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set read
     *
     * @param boolean $read
     * @return Notification
     */
    public function setRead($read)
    {
        $this->read = $read;

        return $this;
    }

    /**
     * Get read
     *
     * @return boolean
     */
    public function getRead()
    {
        return $this->read;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Notification
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Add values
     *
     * @param \Bprs\UserBundle\Entity\NotificationValue $values
     * @return Notification
     */
    public function addValue(NotificationValue $value)
    {
        $this->values[] = $value;
        $value->setNotification($this);

        return $this;
    }

    /**
     * Remove values
     *
     * @param \Bprs\UserBundle\Entity\NotificationValue $values
     */
    public function removeValue(NotificationValue $values)
    {
        $this->values->removeElement($values);
    }

    /**
     * Get values
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getValues($as_array = false)
    {
        if ($as_array) {
            $values = [];
            foreach ($this->values as $value) {
                $values[$value->getNotificationkey()] = $value->getNotificationvalue();
            }
            return $values;
        }
        return $this->values;
    }

    /**
     * Set user
     *
     * @return Notification
     */
    public function setUser($user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    public function getLink()
    {
        return $this->getValues(true)['url'];
    }
}
