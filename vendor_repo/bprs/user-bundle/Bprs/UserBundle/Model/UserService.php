<?php

namespace Bprs\UserBundle\Model;

use Bprs\UserBundle\Entity\User;
use Bprs\UserBundle\Entity\dbRole;
use Bprs\UserBundle\Event\CreatedUserEvent;
use Bprs\UserBundle\Event\PreDeleteUserEvent;
use Bprs\UserBundle\BprsUserEvent;

class UserService
{
    private $em;
    private $mailer;
    private $userclass;
    private $default_roles;
    private $register_header;
    private $dispatcher;

    public function __construct($entityManager, $mailer, $userclass, $default_roles, $register_header, $dispatcher)
    {
        $this->em = $entityManager;
        $this->mailer = $mailer;
        $this->userclass = $userclass;
        $this->default_roles = $default_roles;
        $this->register_header = $register_header;
        $this->dispatcher = $dispatcher;
    }

    public function createUser(User $user, array $role_names = [], $header = false)
    {
        if (!$header) {
            $header = $this->register_header;
        }

        $roles = array_unique(array_merge($this->default_roles, $role_names));
        foreach ($roles as $role_name) {
            $role = $this->getOrCreateRole($role_name);
            $user->addRole($role);
        }

        $hash = openssl_random_pseudo_bytes(10);
        $user->setResetHash(sha1($hash));

        $this->em->persist($user);
        $this->em->flush();

        $this->mailer->sendMail(
            $user->getEmail(),
            '@BprsUser/email/new_user.html.twig',
            ['user' => $user],
            $header
        );
        $event = new CreatedUserEvent($user);
        $this->dispatcher->dispatch(BprsUserEvent::POST_USER_CREATE, $event);
    }

    /**
     * remove user from database. Sends a pre delete event to
     * enable untangling database relations
     */
    public function deleteUser(User $user)
    {
        $event = new PreDeleteUserEvent($user);
        $this->dispatcher->dispatch(BprsUserEvent::PRE_USER_DELETE, $event);

        $this->em->remove($user);
        foreach ($user->getRoles() as $role) {
            $user->removeRole($role);
        }
        $this->em->flush();
    }

    public function addRole(User $user, $role)
    {
        $role = $this->getOrCreateRole($role);
        $user->addRole($role);
        $this->em->persist($user);
        $this->em->flush();
    }

    public function getUser($username)
    {
        return $this->getUserBy(['username' => $username]);
    }

    public function getUserBy($info)
    {
        return $this->em->getRepository($this->userclass)->findOneBy($info);
    }

    public function getOrCreateRole($role_name)
    {
        $role = $this->em->getRepository('BprsUserBundle:Role')->findOneBy(['name' => $role_name]);
        if (!$role) {
            $role = new dbRole();
            $role->setName($role_name);
            $this->em->persist($role);
        }
        return $role;
    }
}
