<?php

namespace Bprs\UserBundle\Model;

use Bprs\UserBundle\Entity\User;
use Bprs\UserBundle\BprsUserEvent;
use Bprs\UserBundle\Event\ResetedUserPasswordEvent;

class iForgotService
{
    private $repo;
    private $mailer;
    private $templating;
    private $em;
    private $iforgot_header;
    private $dispatcher;

    public function __construct($userRepository, $mailer, $passwEnc, $em, $iforgot_header, $dispatcher)
    {
        $this->repo = $userRepository;
        $this->mailer = $mailer;
        $this->password_encoder = $passwEnc;
        $this->em = $em;
        $this->iforgot_header = $iforgot_header;
        $this->dispatcher = $dispatcher;
    }

    /**
     * Prepares user for new password and sends email if done.
     */
    public function resetPassword($username, $header = false)
    {
        if (!$header) {
            $header = $this->iforgot_header;
        }

        $user = $this->repo->findOneBy(array('username' => $username));

        if ($user) {
            $hash = openssl_random_pseudo_bytes(10);
            $user->setResetHash(sha1($hash));
            $this->em->persist($user);
            $this->em->flush();

            $this->mailer->sendMail(
                $user->getEmail(),
                '@BprsUser/email/iforgot.html.twig',
                array('user' => $user),
                $header
            );
        }
    }

    /**
     * sets new password and removes hash to disable old link
     */
    public function setPassword($password, $resetHash)
    {
        $user = $this->repo->findOneBy(array('resetHash' => $resetHash));

        if ($user) {
            $password = $this->password_encoder->encodePassword($user, $password);
            $user->setPassword($password);
            $user->setResetHash(null);
            $this->em->persist($user);
            $this->em->flush();
            $event = new ResetedUserPasswordEvent($user);
            $this->dispatcher->dispatch(BprsUserEvent::POST_PASSWORD_RESET, $event);
        }
    }
}
