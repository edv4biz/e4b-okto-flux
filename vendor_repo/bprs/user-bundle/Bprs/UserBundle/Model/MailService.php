<?php

namespace Bprs\UserBundle\Model;

class MailService
{
    private $mailer;
    private $templating;
    private $from;
    private $from_name;

    public function __construct($mailer, $templating, $from, $from_name)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->from = $from;
        $this->from_name = $from_name;
    }

    /**
     * Sends a mail with your desired template.
     * @param  string $to the mail adress you like to send this email
     * @param  string $template  Something like BprsUserBundle:Email:new_user.html.twig
     * @param  array  $arguments an array with all needed things for your template (don't forget _locale if needed)
     * @param  string $subject subject of your email
     */
    public function sendMail($to, $template, $arguments = array(), $subject = "Nachricht")
    {
        $message = (new \Swift_Message($subject))
            ->setFrom([$this->from => $this->from_name])
            ->setTo($to)
            ->setBody(
                $this->templating->render(
                    $template,
                    $arguments
                ),
                'text/html'
            );

        $this->mailer->send($message);
    }

    public function createMail($to, $subject = "Message")
    {
        return (new \Swift_Message($subject))
            ->setFrom([$this->from => $this->from_name])
            ->setTo($to);
    }

    public function send($message)
    {
        $this->mailer->send($message);
    }
}
