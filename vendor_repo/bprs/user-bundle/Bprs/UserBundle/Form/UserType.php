<?php

namespace Bprs\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'username',
                TextType::class,
                [
                    'label' => 'bprs_user.user.name',
                    'attr' => ['placeholder' => 'bprs_user.user.name_placeholder']
                ]
            )
            ->add(
                'email',
                EmailType::class,
                [
                    'label' => 'bprs_user.user.email',
                    'attr' => ['placeholder' => 'bprs_user.user.email_placeholder']
                ]
            )
            ->add(
                'isActive',
                CheckboxType::class,
                [
                    'label' => 'bprs_user.user.isActive',
                    'required' => false
                ]
            )
            ->add(
                'roles',
                EntityType::class,
                [
                    'class' => 'BprsUserBundle:dbRole',
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('r')->orderBy('r.name', 'ASC');
                    },
                    'multiple' => true,
                    'expanded' => true,
                    'label'   => 'bprs_user.user.roles_label'
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
                'data_class' => 'Bprs\UserBundle\Entity\User'
            ]);
    }

    public function getBlockPrefix()
    {
        return 'BprsUserBundle_usertype';
    }
}
