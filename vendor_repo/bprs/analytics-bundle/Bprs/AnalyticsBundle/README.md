# BprsAnalyticsBundle
Symfony Bundle to track clicks for sites, videos and more

# Installation

```
composer require bprs/analytics-bundle
```

AppKernel:

```
...
new Bprs\AnalyticsBundle\BprsAnalyticsBundle(),
...
```

# Usage
Track Clicks and other Informations

## Controller
via service
```
    /**
     * @Route("/", name="homepage")
     * @Template()
     */
    public function homepageAction(Request $request)
    {
        $this->get('bprs_analytics')->trackInfo($request, "homepage");
        ...
```

## View (Twig)
via javascript (jquery)
```
$.ajax({
    url: "{{ url('bprs_analytics_write_log')}}",
    data: {'identifier': "homepage", 'value': 'visit'}
});
```

Update Logstates with Browser info from the Commandline  
Tip: add the browscap.ini to your php if it is empty!

```
bprs:analytics:update_logstates
```

Send out E-Mail with all Infos in a given Timespan

```
bprs:analytics:export your@email.adress homepage --start="-8 days" --end="-1 day"
```

## Event & Listener
Event will be send for all trackInfo() calls (see above). Just listen to the event with the given identifier


```
logstate_event_listener:
    class: AppBundle\Event\ClickLogstateEventListener
    arguments: [ "@doctrine.orm.entity_manager" ]
    tags:
        - { name: kernel.event_listener, event: start, method: onEpisodeStart }
        - { name: kernel.event_listener, event: start_playlist, method: onPlaylistStart }
```
