<?php

namespace Bprs\AnalyticsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Security\Core\Encoder\BCryptPasswordEncoder;
use GeoIp2\Exception\AddressNotFoundException;
use GeoIp2\Exception\InvalidArgumentException;

class UpdateLogstatesCommand extends ContainerAwareCommand {

    public function __construct()
    {
        parent::__construct();
    }

    protected function configure() {
        $this
            ->setName('bprs:analytics:update_logstates')
            ->setDescription('Updates Logstates, sets platform, browser, parent, version, majorver, minorver, cssversion, bot and mobile infos. Can be run every few minutes');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $logstates = $em->getRepository('BprsAnalyticsBundle:Logstate')->getLogstatesToUpdate(true)->iterate();
        $reader = $this->getContainer()->get('bprs_analytics_geoIP2_reader');

        $output->writeln('start updating logstates');
        $counter = 0;
        foreach ($logstates as $row) {
            $logstate = $row[0];
            $browser = get_browser($logstate->getUserAgent());
            $logstate->setPlatform($browser->platform);
            $logstate->setBrowser($browser->browser);
            $logstate->setParent($browser->parent);
            $logstate->setVersion($browser->version);
            $logstate->setMajorver($browser->majorver);
            $logstate->setMinorver($browser->minorver);
            $logstate->setCssversion($browser->cssversion);
            $logstate->setBotInfo();
            $logstate->setMobileInfo();

            $em->persist($logstate);

            $output->write('.');
            $counter++;
            if (($counter % 50) === 0) {
                $em->flush();
                $em->clear();
            }
        }
        $em->flush();
        $em->clear();
        $output->writeln(sprintf('finished updating [%s] logstate ips', $counter));
    }
}
