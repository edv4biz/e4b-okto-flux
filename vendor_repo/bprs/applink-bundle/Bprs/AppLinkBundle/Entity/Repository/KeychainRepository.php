<?php

namespace Bprs\AppLinkBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class KeychainRepository  extends EntityRepository {

    public function findOneByUniqID($uniqID, $query_only = false)
    {
        $query = $this->getEntityManager()->createQuery(
                'SELECT k FROM BprsAppLinkBundle:Keychain k WHERE k.uniqID = :uniqID'
            );
        $query->setParameter('uniqID', $uniqID);

        if ($query_only) {
            return $query;
        }
        return $query->getSingleResult();
    }

    public function findByRole($role, $query_only = false)
    {
        $query = $this->getEntityManager()->createQuery(
                'SELECT k FROM BprsAppLinkBundle:Keychain k
                LEFT JOIN k.keys ks
                WHERE ks.role = :role'
            )->setParameter('role', $role);

        if ($query_only) {
            return $query;
        }
        return $query->getResult();
    }
}
