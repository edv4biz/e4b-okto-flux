<?php

namespace Bprs\AppLinkBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Bprs\AppLinkBundle\Entity\Key;

class AppLinkAddKeyCommand extends ContainerAwareCommand
{
    public function __construct() {
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('bprs:applinkbundle:add_key')
            ->setDescription('Add key to given keychain')
            ->addArgument(
                'user',
                InputArgument::REQUIRED,
                'What keychain do you want to add a key to?'
            )->addArgument(
                'key',
                InputArgument::REQUIRED,
                'Whats the key?'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->getContainer()->get('bprs_applink')->addKey($input->getArgument('key'), $input->getArgument('user'));
        $output->writeln("Added key to keychain.");
    }
}
