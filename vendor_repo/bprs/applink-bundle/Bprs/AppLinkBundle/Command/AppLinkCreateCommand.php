<?php

namespace Bprs\AppLinkBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Bprs\AppLinkBundle\Entity\Keychain;

class AppLinkCreateCommand extends ContainerAwareCommand
{
    public function __construct() {
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('bprs:applinkbundle:create_key')
            ->setDescription('Create a new Keychain')
            ->addArgument(
                'url',
                InputArgument::REQUIRED,
                'What application do you want to link with?'
            )->addArgument(
                'user',
                InputArgument::REQUIRED,
                'What application do you want to link with?'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $keychain = new Keychain();
        $keychain->setUrl($input->getArgument('url'));
        $keychain->setUser($input->getArgument('user'));

        $this->getContainer()->get('logger')->info('Creating new key from commandline for '.$keychain->getUrl());
        $this->getContainer()->get('bprs_applink')->createKeychain($keychain);
        $output->writeln("Finished keychain! You can now start adding keys to it.");
    }
}
