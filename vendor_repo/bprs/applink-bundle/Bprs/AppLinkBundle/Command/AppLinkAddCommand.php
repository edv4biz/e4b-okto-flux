<?php

namespace Bprs\AppLinkBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Bprs\AppLinkBundle\Entity\Keychain;

class AppLinkAddCommand extends ContainerAwareCommand
{
    public function __construct() {
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('bprs:applinkbundle:add_keychain')
            ->setDescription('Add a remote Keychain')
            ->addArgument(
                'url',
                InputArgument::REQUIRED,
                'What application do you want to link with?'
            )->addArgument(
                'user',
                InputArgument::REQUIRED,
                'Whats the user for your keychain?'
            )->addArgument(
                'api_key',
                InputArgument::REQUIRED,
                'The api key (ask the owner of the remote application)'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $keychain = new Keychain();
        $keychain->setUrl($input->getArgument('url'));
        $keychain->setUser($input->getArgument('user'));
        $keychain->setApiKey($input->getArgument('api_key'));

        $this->getContainer()->get('logger')->info('Add key from commandline for '.$keychain->getUrl());
        $this->getContainer()->get('bprs_applink')->addKeychain($keychain);
        $output->writeln("Finished keychain! You can now start adding keys to it.");
    }
}
