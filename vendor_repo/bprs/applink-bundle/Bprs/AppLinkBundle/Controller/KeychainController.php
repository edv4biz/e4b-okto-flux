<?php

namespace Bprs\AppLinkBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Bprs\AppLinkBundle\Entity\Keychain;
use Bprs\AppLinkBundle\Entity\Key;
use Bprs\AppLinkBundle\Form\KeychainType;
use Bprs\AppLinkBundle\Form\KeyType;
use Bprs\AppLinkBundle\Form\NewKeychainType;
use Bprs\AppLinkBundle\Form\AcceptKeychainType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

/**
 * @Route("/backend/bprs_applink/keychain")
 * @Security("is_granted('ROLE_BPRS_APPLINK_BACKEND')")
 */
class KeychainController extends Controller
{
    /**
     * @Route("/{page}", name="bprs_applink_keychains", defaults={"page" = 1}, requirements={"page": "\d+"})
     * @Template()
     */
    public function indexAction($page = 1)
    {
        $dql = "SELECT k FROM BprsAppLinkBundle:Keychain k";
        $query = $this->getDoctrine()->getManager()->createQuery($dql);
        $paginator  = $this->get('knp_paginator');
        $keychains = $paginator->paginate(
            $query,
            $page,
            5
        );

        return array('keychains' => $keychains);
    }

    /**
     * @Route("/new", name="bprs_applink_keychain_new")
     * @Template()
     */
    public function newAction(Request $request)
    {
        $keychain = new Keychain();
        $keychain->setIsActive(true);
        $form = $this->createForm(NewKeychainType::class, $keychain);
        $form->add('submit', SubmitType::class, ['label' => 'bprs_applink.keychain_new_button', 'attr' => ['class' => 'btn btn-primary']]);

        if ($request->getMethod() == "POST") { //sends form
            $form->handleRequest($request);
            $em = $this->getDoctrine()->getManager();
            if ($form->isValid()) { //form is valid, save or preview
                if ($form->get('submit')->isClicked()) { //save me
                    try {
                        $this->get('bprs_applink')->createKeychain($keychain);
                        $this->get('session')->getFlashBag()->add('success', 'bprs_applink.success_create_keychain');
                        return $this->redirect($this->generateUrl('bprs_applink_keychain_show', ['uniqID' => $keychain->getUniqID()]));
                    } catch (Exception $e) {
                        $this->get('session')->getFlashBag()->add('error', 'bprs_applink.error_check_create_keychain');
                        return $this->redirect($this->generateUrl('bprs_applink_keychain_new'));
                    }
                } else { //???
                    $this->get('session')->getFlashBag()->add('info', 'bprs_applink.info_edit_keychain_unknown_action');
                    return $this->redirect($this->generateUrl('bprs_applink_keychains'));
                }
            }
            $this->get('session')->getFlashBag()->add('error', 'bprs_applink.error_create_keychain');
        }

        return ['form' => $form->createView()];
    }

    /**
     * @Route("/{uniqID}/edit", name="bprs_applink_keychain_edit")
     * @Template()
     */
    public function editAction(Request $request, Keychain $keychain)
    {
        $form = $this->createForm(KeychainType::class, $keychain);
        $form->add('submit', SubmitType::class, ['label' => 'bprs_applink.keychain_edit_button', 'attr' => ['class' => 'btn btn-primary']]);
        $form->add('delete', SubmitType::class, ['label' => 'bprs_applink.keychain_delete_button', 'attr' => ['class' => 'btn btn-danger']]);

        if ($request->getMethod() == "POST") { //sends form
            $form->handleRequest($request);
            $em = $this->getDoctrine()->getManager();
            if ($form->isValid()) { //form is valid, save or preview
                if ($form->get('submit')->isClicked()) { //save me
                    $em->persist($keychain);
                    $em->flush();
                    $this->get('session')->getFlashBag()->add('success', 'bprs_applink.success_edit_keychain');
                    return $this->redirect($this->generateUrl('bprs_applink_keychain_show', ['uniqID' => $keychain->getUniqID()]));
                } elseif ($form->get('delete')->isClicked()) {
                    $em->remove($keychain);
                    $em->flush();
                    $this->get('session')->getFlashBag()->add('success', 'bprs_applink.success_delete_keychain');
                    return $this->redirect($this->generateUrl('backend'));
                } else { //???
                    $this->get('session')->getFlashBag()->add('info', 'bprs_applink.info_edit_keychain_unknown_action');
                    return $this->redirect($this->generateUrl('bprs_applink_keychain_show', ['uniqID' => $keychain->getUniqID()]));
                }
            }
            $this->get('session')->getFlashBag()->add('error', 'bprs_applink.error_edit_keychain');
        }

        return ['form' => $form->createView()];
    }

    /**
     * @Route("/{uniqID}/show", name="bprs_applink_keychain_show")
     * @Template()
     */
    public function showAction(Keychain $keychain)
    {
        return ['keychain' => $keychain];
    }

    /**
     * @Route("/accept", name="bprs_applink_keychain_accept")
     * @Template()
     */
    public function acceptAction(Request $request)
    {
        $keychain = new Keychain();
        $keychain->setIsActive(true);
        $keychain->setApiKey('');
        $form = $this->createForm(AcceptKeychainType::class, $keychain);
        $form->add('submit', SubmitType::class, ['label' => 'bprs_applink.keychain_accept_button', 'attr' => ['class' => 'btn btn-primary']]);

        if ($request->getMethod() == "POST") { //sends form
            $form->handleRequest($request);
            $em = $this->getDoctrine()->getManager();
            if ($form->isValid()) { //form is valid, save or preview
                if ($form->get('submit')->isClicked()) { //save me
                    try {
                        $this->get('bprs_applink')->addKeychain($keychain);
                        $this->get('session')->getFlashBag()->add('success', 'bprs_applink.success_accept_keychain');
                        return $this->redirect($this->generateUrl('bprs_applink_keychain_show', ['uniqID' => $keychain->getUniqID()]));
                    } catch (Exception $e) {
                        $this->get('session')->getFlashBag()->add('error', 'bprs_applink.error_check_accept_keychain');
                        return $this->redirect($this->generateUrl('bprs_applink_keychain_accept'));
                    }
                } else { //???
                    $this->get('session')->getFlashBag()->add('info', 'bprs_applink.info_accept_keychain_unknown_action');
                    return $this->redirect($this->generateUrl('bprs_applink_keychains'));
                }
            }
            $this->get('session')->getFlashBag()->add('error', 'bprs_applink.error_accept_keychain');
        }

        return ['form' => $form->createView()];
    }

    /**
     * @Route("/{uniqID}/add_key", name="bprs_applink_keychain_addkey")
     * @Template()
     */
    public function addKeyAction(Request $request, Keychain $keychain)
    {
        $key = new Key();
        $form = $this->createForm(KeyType::class, $key);
        $form->add('submit', SubmitType::class, ['label' => 'bprs_applink.key_add_button', 'attr' => ['class' => 'btn btn-primary']]);

        if ($request->getMethod() == "POST") { //sends form
            $form->handleRequest($request);
            $em = $this->getDoctrine()->getManager();
            if ($form->isValid()) { //form is valid, save or preview
                if ($form->get('submit')->isClicked()) { //save me
                    $keychain->addKey($key);
                    $em->persist($key);
                    $em->persist($keychain);
                    $em->flush();
                    $this->get('session')->getFlashBag()->add('success', 'bprs_applink.success_add_key');
                    return $this->redirect($this->generateUrl('bprs_applink_keychain_show', ['uniqID' => $keychain->getUniqID()]));
                } else { //???
                    $this->get('session')->getFlashBag()->add('info', 'bprs_applink.info_add_key_unknown_action');
                    return $this->redirect($this->generateUrl('bprs_applink_keychains'));
                }
            }
            $this->get('session')->getFlashBag()->add('error', 'bprs_applink.error_add_key');
        }

        return ['form' => $form->createView(), 'keychain' => $keychain];
    }
}
