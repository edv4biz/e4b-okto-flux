<?php

namespace Bprs\AppLinkBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends Controller
{
    /**
     *
     * @Route("/api/bprs_applink/interface", name="bprs_applink_interface")
     */
    public function checkMeAction(Request $request)
    {
        $action = $request->query->get('action', 'check');
        switch ($action) {
            case 'check':
                $this->denyAccessUnlessGranted('ROLE_BPRS_APPLINK', null, 'Unable to access this page!');
                return new Response("", Response::HTTP_OK);
            case 'read_roles':
                $this->denyAccessUnlessGranted('ROLE_BPRS_APPLINK', null, 'Unable to access this page!');
                $apiuser = $this->get('security.context')->getToken()->getUser();
                return new JsonResponse(['roles' => $apiuser->getRoles()]);
            case 'read_api':
                $this->denyAccessUnlessGranted('ROLE_BPRS_APPLINK', null, 'Unable to access this page!');
                $urls = $this->get('bprs_applinkhelper')->getAvailableUrls();
                return new JsonResponse($urls);
            default:
                return new Response("", Response::HTTP_BAD_REQUEST);
                break;
        }
    }
}
