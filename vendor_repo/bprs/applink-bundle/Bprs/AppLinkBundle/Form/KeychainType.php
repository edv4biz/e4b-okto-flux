<?php

namespace Bprs\AppLinkBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class KeychainType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user', TextType::class, ['label' => 'bprs_applink.keychain_user_label'])
            ->add('api_key', TextType::class, ['label' => 'bprs_applink.keychain_apiKey_label'])
            ->add('url', UrlType::class, ['label' => 'bprs_applink.keychain_url_label'])
            ->add('isActive', CheckboxType::class, ['label' => 'bprs_applink.keychain_isActive_label'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => "Bprs\AppLinkBundle\Entity\Keychain"
        ]);
    }

    public function getBlockPrefix()
    {
        return 'bprs_applink_keychain';
    }
}
