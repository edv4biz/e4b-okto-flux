<?php

namespace Bprs\AppLinkBundle\Twig;

class KeychainExtension extends \Twig_Extension
{
    private $applink;

    public function __construct($applink)
    {
        $this->applink = $applink;
    }


    public function getFilters() {
        return [
            new \Twig_SimpleFilter('keychain_url', [$this,'keychainUrlFilter'])
        ];
    }

    public function keychainUrlFilter($keychain, $url, $queries = null)
    {
        if ($queries) {
            $link = $this->applink->getApiUrlsForKey($keychain, $url);
            return $this->appendQuery($link, $queries);
        }
        return $this->applink->getApiUrlsForKey($keychain, $url);
    }

    public function getName() {
        return 'bprs_applink_extension';
    }

    public function appendQuery($url, array $queries)
    {
        $url = parse_url($url, PHP_URL_QUERY)
            ? $url.'&'
            : $url.'?'
        ;
        $first = true;
        foreach ($queries as $key => $value) {
            if ($first) {
                $url = sprintf('%s%s=%s', $url, $key, $value);
                $first = false;
            } else {
                $url = sprintf('%s&%s=%s', $url, $key, $value);
            }
        }

        return $url;
    }
}
