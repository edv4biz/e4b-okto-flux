<?php

namespace Bprs\AppLinkBundle\Model;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Bprs\AppLinkBundle\Entity\Key;
use Bprs\AppLinkBundle\Entity\Keychain;
use Bprs\AppLinkBundle\Entity\Bundle;
use GuzzleHttp\Client;
use Bprs\AppLinkeBundle\BprsAppLinkUrlInterface;

/**
 * Allows creating and updating of AppLinks
 */
class AppLinkService
{
    private $em;
    private $logger;
    private $serializer;
    private $available_urls;

    public function __construct($entityManager, $logger, $serializer)
    {
        $this->em = $entityManager;
        $this->logger = $logger;
        $this->serializer = $serializer;
    }

    /**
     * Creates brand new keychain and checks if url is correct
     * @param  App    $app The App you want to use
     * @return [type]      [description]
     */
    public function createKeychain(Keychain $keychain)
    {
        $this->logger->info('createKeychain started');
        //send our key
        $client = new Client(['verify' => false ]);
        //the url is correct, the keychain can be used if created correctly on the other side
        try {
            $response = $client->request('GET', $keychain->getUrl());
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            if ($e->getResponse()->getStatusCode() == Response::HTTP_UNAUTHORIZED) {
                $key = new Key();
                $key->setRole('ROLE_BPRS_APPLINK');
                $key->setKeychain($keychain);
                $keychain->addKey($key);
                $this->em->persist($keychain);
                $this->em->persist($key);
                $this->em->flush();
            } else {
                throw new \Exception(sprintf("Response of %s was: %s", $keychain->getUrl(), $response->getStatusCode()));
            }
        }
    }

    /**
    * Adds a (remote) keychain and checks if username/key is correct
    */
    public function addKeychain(Keychain $keychain)
    {
        $this->logger->info('addKeychain started');
        $client = new Client(['verify' => false ]);
        $response = $client->request(
            'GET',
            $keychain->getUrl(),
            [
                'auth' => [$keychain->getUser(), $keychain->getApiKey()],
                'query' => ['action' => 'check']
            ]
        );

        if ($response->getStatusCode() == Response::HTTP_OK) {
            $key = new Key();
            $key->setRole('ROLE_BPRS_APPLINK');
            $key->setKeychain($keychain);
            $keychain->addKey($key);
            $this->em->persist($keychain);
            $this->em->persist($key);
            $this->em->flush();
        } else {
            throw new \Exception(sprintf("Response of %s was: %s", $keychain->getUrl(), $response->getStatusCode()));
        }
    }

    public function addKey($role, $name)
    {
        $keychain = $this->em->getRepository('BprsAppLinkBundle:Keychain')->findOneBy(['user' => $name]);

        $key = new Key();
        $key->setKeychain($keychain);
        $key->setRole($role);
        $this->em->persist($key);
        $this->em->persist($keychain);
        $this->em->flush();
    }

    public function getApiUrlsForKey(Keychain $keychain, $route_name = false)
    {
        $client = new Client(['verify' => false ]);
        $response = $client->request(
            'GET',
            $keychain->getUrl(),
            [
                'auth' => [$keychain->getUser(), $keychain->getApiKey()],
                'query'=> ['action' => 'read_api']
            ]
        );

        if ($response->getStatusCode() == Response::HTTP_OK) {
            $routes = json_decode($response->getBody(), true);
            if ($route_name) {
                if (array_key_exists($route_name, $routes)) {
                    return $routes[$route_name];
                }
                return false;
            }
            return $routes;
        }

        $this->logger->info('could not read apiUrlsForKey!');
        return false;
    }

    public function getKeychainsWithRole($role)
    {
        return $this->em->getRepository('BprsAppLinkBundle:Keychain')->findByRole($role);
    }

    public function getKeychains()
    {
        return $this->em->getRepository('BprsAppLinkBundle:Keychain')->findAll();
    }

    public function getKeychain($uniqID)
    {
        return $this->em->getRepository('BprsAppLinkBundle:Keychain')->findOneBy(['uniqID' => $uniqID]);
    }
}
