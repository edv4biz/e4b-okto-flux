<?php

namespace Bprs\AssetBundle;

final class BprsAssetEvent
{
    /**
     * The bprs_asset.pre_create event is thrown each time before an asset is created
     * in the system.
     *
     * The event listener receives an
     * Bprs\AssetBundle\Event\CreateAssetEvent instance. Includes the asset and the event.
     *
     * @var string
     */
    const ASSET_PRECREATE = 'bprs_asset.pre_create';

    /**
     * The bprs_asset.post_create event is thrown each time after an asset is created
     * in the system.
     *
     * The event listener receives an
     * Bprs\AssetBundle\Event\CreateAssetEvent instance. Includes the asset and the event
     *
     * @var string
     */
    const ASSET_POSTCREATE = 'bprs_asset.post_create';

    /**
     * The bprs_asset.delete event is thrown each time before an asset is deleted
     * in the system.
     *
     * The event listener receives an
     * Bprs\AssetBundle\Event\DeleteAssetEvent instance.
     *
     * @var string
     */
    const ASSET_DELETE = 'bprs_asset.delete';

    /**
    * The bprs_asset.missing event is thrown each time before the 404
    * placeholder for an filesystem is requested.
    *
    * The event listener receives an
    * Bprs\AssetBundle\Event\MissingAssetEvent instance.
    *
    * @var string
    */
    const ASSET_MISSING = 'bprs_asset.missing';

    /**
     * The bprs_asset.updated event is dispatched each time an asset FILE gets
     * (physically) modified. (for example cropped).
     * Usefull for MD5 hash updates, logging, etc.
     */
    const ASSET_UPDATED = 'bprs_asset.update';
}
