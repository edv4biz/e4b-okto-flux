<?php

namespace Bprs\AssetBundle\Model;

class AssetJobService
{
    private $jobservice;
    private $worker_queue;

    public function __construct($JobService, $worker_queue)
    {
        $this->jobservice = $JobService;
        $this->worker_queue = $worker_queue;
    }

    public function addMoveAssetJob($asset, $adapter, $worker_queue = false) {
        if (!$worker_queue) {
            $worker_queue = $this->worker_queue;
        }

        $this->jobservice->addJob(
            "Bprs\AssetBundle\Model\MoveAssetJob",
            [
                'filekey' => $asset->getFilekey(),
                'adapter' => $adapter
            ],
            $worker_queue
        );
    }

    public function addMD5Job($asset, $worker_queue = false) {
        if (!$worker_queue) {
            $worker_queue = $this->worker_queue;
        }

        $this->jobservice->addJob(
            "Bprs\AssetBundle\Model\MD5Job",
            ['filekey' => $asset->getFilekey()],
            $this->worker_queue
        );
    }

    public function addFilesizeJob($asset, $worker_queue = false) {
        if (!$worker_queue) {
            $worker_queue = $this->worker_queue;
        }

        $this->jobservice->addJob(
            "Bprs\AssetBundle\Model\FilesizeJob",
            [
                'filekey' => $asset->getFilekey()
            ],
            $this->worker_queue
        );
    }
}

?>
