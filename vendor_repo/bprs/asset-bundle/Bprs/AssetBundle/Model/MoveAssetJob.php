<?php

namespace Bprs\AssetBundle\Model;

use Bprs\CommandLineBundle\Model\BprsContainerAwareJob as BaseJob;
use Bprs\AssetBundle\Entity\Asset;

class MoveAssetJob extends BaseJob {

    /**
     * moves a file from one adapter to another using gaufrette or flysystem
     * dispatches an assetUpdate event
     */
    public function perform() {
        $asset_service = $this->getContainer()->get('bprs.asset');
        $asset = $asset_service->getAsset($this->args['filekey']);
        $asset_service->moveAsset($asset, $this->args['adapter']);
    }

    public function getName() {
        return 'File Copy Job';
    }
}
?>
