<?php

namespace Bprs\AssetBundle\Model;

use Bprs\AssetBundle\Entity\Asset;
use Bprs\AssetBundle\Event\DeleteAssetEvent;
use Bprs\AssetBundle\Event\CreateAssetEvent;
use Bprs\AssetBundle\Event\UpdatedAssetEvent;
use Bprs\AssetBundle\Event\MissingAssetEvent;
use Bprs\AssetBundle\Event\MovedAssetEvent;
use Bprs\AssetBundle\BprsAssetEvent;

class AssetHelperService
{
    private $assetMapper;
    private $filesystemMapper;
    private $dispatcher;
    private $placeholder_404;
    private $thumb_filesystem;

    public function __construct(
        $assetMapper,
        $filesystemMapper,
        $dispatcher,
        $placeholder_404,
        $thumb_filesystem
    ) {
        $this->assetMapper = $assetMapper;
        $this->filesystemMapper = $filesystemMapper;
        $this->dispatcher = $dispatcher;
        $this->placeholder_404 = $placeholder_404;
        $this->thumb_filesystem = $thumb_filesystem;
    }

    public function isImage($asset) {
        if ($asset->getMimetype() == 'image/png' || $asset->getMimetype() == 'image/jpeg') {
            return true;
        }
        return false;
    }

    public function isRemote(Asset $asset)
    {
        if (key_exists('remote', $this->assetMapper[$asset->getAdapter()])) {
            return $this->assetMapper[$asset->getAdapter()]['remote'];
        }
        return false;
    }

    public function getAbsoluteUrl(Asset $asset, $use_fallback = false) {
        $filesystem = $this->getFilesystem($asset->getAdapter());
        if ($filesystem->has($asset->getFilekey()) && array_key_exists('url', $this->assetMapper[$asset->getAdapter()])) {
            return $this->assetMapper[$asset->getAdapter()]['url'].'/'.$asset->getFilekey();
        }
        if ($use_fallback) {
            return $this->get404($asset);
        }
        return false;
    }

    public function getPath(Asset $asset, $ignore_missing = false) {
        $filesystem = $this->getFilesystem($asset->getAdapter());
        if ($ignore_missing) {
            return $this->assetMapper[$asset->getAdapter()]['path'].'/'.$asset->getFilekey();
        }
        if ($filesystem->has($asset->getFilekey())) {
            return $this->assetMapper[$asset->getAdapter()]['path'].'/'.$asset->getFilekey();
        }
    }

    public function getThumbnail($asset, $height, $witdh)
    {
        return $this->createThumbnail($asset, $height, $witdh);
    }

    public function cropAsset(Asset $asset, $x, $y, $width, $height) {
        $filesystem = $this->getFilesystem($asset->getAdapter());
        if ($filesystem->has($asset->getFilekey())) {
            $image = false;
            switch ($asset->getMimeType()) {
                case 'image/png':
                    $image = \imagecreatefrompng($this->getAbsoluteUrl($asset));
                    break;
                case 'image/jpeg':
                    $image = \imagecreatefromjpeg($this->getAbsoluteUrl($asset));
                    break;
            }
            $image = \imagecrop($image, ['x' => $x, 'y' => $y, 'width' => $width, 'height' => $height]);
            ob_start();
                header(sprintf('Content-Type: %s', $asset->getMimeType()));
                switch ($asset->getMimeType()) {
                    case 'image/png':
                        imagepng($image);
                        break;
                    case 'image/jpeg':
                        imagejpeg($image);
                        break;
                }

                $crop = ob_get_contents();
            ob_end_clean();

            $filesystem->delete($asset->getFilekey());
            $filesystem->write($asset->getFilekey(), $crop);

            $this->dispatchUpdatedAssetEvent($asset);
        } else {
            $this->dispatchMissingEvent($asset);
        }
    }

    public function deleteThumbnails($asset)
    {
        $filesystem = $this->getFilesystem($asset->getAdapter());
        $contents = $filesystem->listContents();
        foreach ($contents as $object) {
            if (
                strpos($object['path'], $asset->getFilekey(true).'_thumbnail_') == true ||
                strpos($object['path'], $asset->getFilekey(true)."_thumbnail_") === 0
                ) {
                $filesystem->delete($object['path']);
            }
        }
    }

    /**
     * Deletes file and its thumbnails from the filesystem
     * and dispatches asset delete event.
     * listeners have the chance to remove relations to the asset
     */
    public function deleteAsset(Asset $asset) {
        $filesystem = $this->getFilesystem($asset->getAdapter());

        if ($filesystem->has($asset->getFilekey())) {
            $filesystem->delete($asset->getFilekey());
        }
        $this->deleteThumbnails($asset);
    }

    public function get404($asset = null) {
        if (!$asset) {
            return $this->placeholder_404;
        }

        $this->dispatchMissingEvent($asset);
        if (key_exists($asset->getAdapter(), $this->assetMapper)) {
            if ( key_exists('404', $this->assetMapper[$asset->getAdapter()])) {
                return $this->assetMapper[$asset->getAdapter()]['404'];
            }
        }
        return $this->placeholder_404;
    }

    public function createThumbnail($asset, $height, $width) {
        if (!$asset) {
            $asset = new Asset();
            $asset->setFilekey('404');
            $asset->setAdapter($this->thumb_filesystem);
            $asset->setMimetype(image_type_to_mime_type(exif_imagetype($this->getAbsoluteUrl($asset, true))));
        }
        $filesystem = $this->getFilesystem($asset->getAdapter());
        $thumbkey = sprintf('%s_thumbnail_%sx%s.jpg', $asset->getFilekey(true), $height, $width);
        $thumburl = sprintf('%s/%s', $this->assetMapper[$asset->getAdapter()]['url'], $thumbkey);

        if ( $filesystem->has($thumbkey)) { // thumb exists, return url
            return $thumburl;
        }

        // thumb doesn't exist, get original or its 404 fallback
        $absoluteUrl = $this->getAbsoluteUrl($asset, true);
        $mimetype = image_type_to_mime_type(exif_imagetype($absoluteUrl));

        $image = null;
        // thumb does not yet exist. create it and return the public url
        switch ($mimetype) {
            case 'image/png':
                $image = \imagecreatefrompng($absoluteUrl);
                break;
            case 'image/jpeg':
                $image = \imagecreatefromjpeg($absoluteUrl);
                break;
        }

        $thumb_img = imagecreatetruecolor($width, $height);
        // get smaller side
        $multiple = 1;
        if (imagesx($image) > imagesy($image)) { // height is smaller (for example 16:9 posterframe)
            $multiple = imagesy($image)/$height;
        } else {
            $multiple = imagesx($image)/$width;
        }

        imagecopyresampled(
            $thumb_img,
            $image,
            0,
            0,
            ((imagesx($image) - $width*$multiple)/2),
            ((imagesy($image) - $height*$multiple)/2),
            $width,
            $height,
            $width*$multiple,
            $height*$multiple
        );

        ob_start();
            header('Content-Type: image/jpeg');
            imagejpeg($thumb_img);
            $thumb = ob_get_contents();
        ob_end_clean();

        $filesystem->write($thumbkey, $thumb);

        return $thumburl;
    }

    public function dispatchPreCreateAssetEvent($asset, $event)
    {
        $event = new CreateAssetEvent($asset, $event);
        $this->dispatcher->dispatch(BprsAssetEvent::ASSET_PRECREATE, $event);
    }

    public function dispatchPostCreateAssetEvent($asset, $event)
    {
        $event = new CreateAssetEvent($asset, $event);
        $this->dispatcher->dispatch(BprsAssetEvent::ASSET_POSTCREATE, $event);
    }

    public function dispatchMissingEvent($asset) {
        $event = new MissingAssetEvent($asset);
        $this->dispatcher->dispatch(BprsAssetEvent::ASSET_MISSING, $event);
    }

    public function dispatchMovedAssetEvent($asset) {
        $event = new MovedAssetEvent($asset);
        $this->dispatcher->dispatch(BprsAssetEvent::ASSET_MOVED, $event);
    }

    public function dispatchUpdatedAssetEvent($asset) {
        $event = new UpdatedAssetEvent($asset);
        $this->dispatcher->dispatch(BprsAssetEvent::ASSET_UPDATED, $event);
    }

    public function dispatchDeleteAssetEvent($asset)
    {
        $event = new DeleteAssetEvent($asset);
        $this->dispatcher->dispatch(BprsAssetEvent::ASSET_DELETE, $event);
    }

    public function getFilesystem($adapter) {
        if (get_class($this->filesystemMapper) == "League\Flysystem\MountManager") {
            return $filesystem = $this->filesystemMapper->getFilesystem($adapter);
        } else { // probably gaufrette
            return $filesystem = $this->filesystemMapper->get($adapter);
        }
    }

    public function getFilesystemMapper() {
        return $this->filesystemMapper;
    }

    /**
     *
     */
    public function moveAsset($asset, $adapter)
    {
        if (strcmp($asset->getAdapter(), $adapter) == 0) {
            return false;
        } else {
            if (get_class($this->filesystemMapper) == "League\Flysystem\MountManager") {
                // flysystem
                $this->filesystemMapper->copy(
                    sprintf(
                        '%s://%s',
                        $asset->getAdapter(),
                        $asset->getFilekey()
                    ),
                    sprintf(
                        '%s://%s',
                        $adapter,
                        $asset->getFilekey())
                    );

                $origin_filesystem = $this->getFilesystem($asset->getAdapter());
                $asset->setAdapter($adapter);
                // $em = $this->getContainer()->get('doctrine.orm.entity_manager');
                // $em->persist($asset);
                // $em->flush();

                // delete file from original place
                if ($origin_filesystem->has($asset->getFilekey())) {
                    $origin_filesystem->delete($asset->getFilekey());
                }
                return $asset;

            } else {
                // NOTE: gaufrette can't handle large files without using a lot of RAM (09.02.2016)
                // You can use flysystem wich works well with filestreams
                $origin_filesystem = $this->getFilesystem();
                $origin_filesystem->createStream($asset->getFilekey());
                $dest_filesystem->createStream($asset->getFilekey());
                $dest_filesystem->write(
                    $asset->getFilekey(),
                    $origin_filesystem->read($asset->getFilekey())
                );

                $asset->setAdapter($adapter);
                return $asset;
                // $em = $this->getContainer()->get('doctrine.orm.manager');
                // $em->persist($asset);
                // $em->flush();

                // delete file from original place
                if ($origin_filesystem->has($asset->getFilekey())) {
                    $origin_filesystem->delete($asset->getFilekey());
                }

            }
            return true;
        }
    }
}
