<?php

namespace Bprs\AssetBundle\Entity;

use Doctrine\ORM\EntityRepository;

class AssetRepository extends EntityRepository {

    public function findAssetsFromAdapter($asset_class = null, $number = 5, $adapter, $query_only = false)
    {
        $query = null;
        if ($asset_class) {
        $query = $this->getEntityManager()
            ->createQuery('SELECT a FROM '.$asset_class.' a WHERE a.adapter = :adapter ORDER BY a.created_at DESC')
            ->setParameter('adapter', $adapter);
        } else {
            $query = $this->getEntityManager()
                ->createQuery('SELECT a FROM '.$asset_class.' a ORDER BY a.created_at DESC');
        }

        if ($query_only) {
            return $query;
        }
        return $query->setMaxResults($number)->getResult();
    }

    public function findForAdapter($asset_class, $adapter = false, $number = 12, $query_only = false)
    {
        $query = null;
        if ($adapter) {
            $query = $this->getEntityManager()
                ->createQuery("SELECT a FROM ".$asset_class." a WHERE a.adapter = :adapter")
                ->setParameter('adapter', $adapter);
        } else {
            $query = $this->getEntityManager()
                ->createQuery("SELECT a FROM ".$asset_class." a");
        }

        if ($query_only) {
            return $query;
        }

        return $query->setMaxResults($number)->getResult();
    }
}
 ?>
