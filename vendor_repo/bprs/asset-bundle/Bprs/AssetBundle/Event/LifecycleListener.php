<?php

namespace Bprs\AssetBundle\Event;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Bprs\AssetBundle\Event\MissingAssetEvent;
use Bprs\AssetBundle\Event\UpdatedAssetEvent;
use Bprs\AssetBundle\Event\DeleteAssetEvent;
use Symfony\Component\DependencyInjection\Container;
use Doctrine\ORM\Mapping as ORM;

class LifecycleListener {

    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function AssetMissing(MissingAssetEvent $event) {
        $logger = $this->container->get('logger');
        $logger->error(
            sprintf(
                'File for Asset [%s](%s) does not exist in the filesystem!',
                $event->getAsset()->getFilekey(),
                $event->getAsset()->getName()
            )
        );
    }

    public function AssetDelete(DeleteAssetEvent $event) {
        $logger = $this->container->get('bprs_logbook');
        $logger->info(
            'bprs_asset.deleted_asset_info',
            ['%filename%' => $asset->getName()],
            $asset->getFilekey()
        );
    }

    /**
     * is used after cropping assets.
     */
    public function AssetUpdate(UpdatedAssetEvent $event) {
        $asset_helper = $this->container->get('bprs.asset_helper');
        $asset_helper->deleteThumbnails($event->getAsset());

        $jobservice = $this->container->get('bprs.asset_job');
        $jobservice->addMD5Job($event->getAsset());
        $jobservice->addFilesizeJob($event->getAsset());
    }

//  - - - Doctrine Lifecycle - - -

    /**
     * @ORM\PostPersist
     * Automaticly writes a logline when the asset is created
     */
    public function postPersistHandler($asset, LifecycleEventArgs $event)
    {
        $logger = $this->container->get('bprs_logbook');
        $logger->info(
            'bprs_asset.created_new_asset_info',
            ['%filename%' => $asset->getName()],
            $asset->getFilekey()
        );
        $jobservice = $this->container->get('bprs.asset_job');
        $jobservice->addMD5Job($asset);
        $jobservice->addFilesizeJob($asset);
    }

    /**
     * @ORM\PostUpdate
     * Automaticly removes old thumbnail data
     * and triggers metainfo jobs for assets
     */
    public function postUpdateHandler($asset, LifecycleEventArgs $event)
    {
        $asset_helper = $this->container->get('bprs.asset_helper');
        $asset_helper->deleteThumbnails($asset);

        $jobservice = $this->container->get('bprs.asset_job');
        if ($asset->getUpdatedAt() < new \Datetime('-5 minutes')) {
            $jobservice->addMD5Job($asset);
            $jobservice->addFilesizeJob($asset);
        }
    }

    /**
     * @ORM\PreRemove
     * create an event to allow other parts of the application to react
     */
    public function preRemoveHandler($asset, LifecycleEventArgs $event)
    {
        $asset_helper = $this->container->get('bprs.asset_helper');
        $asset_helper->dispatchDeleteAssetEvent($asset);
    }

    /**
     * @ORM\PostRemove
     * Automaticly deletes files from the filesystem
     */
    public function postRemoveHandler($asset, LifecycleEventArgs $event)
    {
        $asset_helper = $this->container->get('bprs.asset_helper');
        $asset_helper->deleteAsset($asset);
    }
}
?>
