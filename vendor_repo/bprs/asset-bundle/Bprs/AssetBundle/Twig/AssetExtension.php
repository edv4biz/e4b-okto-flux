<?php

namespace Bprs\AssetBundle\Twig;

use Gaufrette\File;
use Bprs\AssetBundle\Entity\Asset;


class AssetExtension extends \Twig_Extension
{
    private $asset_helper;

    public function __construct($asset_helper) {
        $this->asset_helper = $asset_helper;
    }

    public function getFilters() {
        return array(
            new \Twig_SimpleFilter('path', array($this, 'pathFilter')),
            new \Twig_SimpleFilter('filesize', array($this, 'filesizeFilter')),
            new \Twig_SimpleFilter('thumb', array($this, 'thumbnailFilter')),
            new \Twig_SimpleFilter('isImage', array($this, 'isImageFilter')),
            new \Twig_SimpleFilter('link', array($this, 'linkFilter'))
        );
    }

    /**
     * Returns human readable filesize in IEC (GiB) or JEDEC (GB)
     * @param  object  $asset       the asset
     * @param  boolean $actualsize switch to show result in e.g. GiB or GB
     * @return string              Human readable filesize like "24,38 GB"
     */
    public function filesizeFilter($asset, $actualsize=true) {
        if ($asset instanceof Asset) {
            $filesizeInByte = $asset->getFilesize();
        } else {
            $filesizeInByte = $asset;
        }
        if ($filesizeInByte <= 0 || $filesizeInByte == null ) {
            return '?';
        }
        if ($actualsize) {
            if ($filesizeInByte >= 1000000000) {
                return ( round( ($filesizeInByte/1000000000), 2).' GB' );
            } else if ($filesizeInByte >= 1000000) {
                return ( round( ($filesizeInByte/1000000), 2).' MB' );
            } else if ($filesizeInByte >= 1000) {
                return ( round( ($filesizeInByte/1000), 2).' KB' );
            } else {
                return $filesizeInByte.' B';
            }
        } else {
            if ($filesizeInByte >= 1073741824) {
                return ( round( ($filesizeInByte/1073741824), 2).' GiB' );
            } else if ($filesizeInByte >= 1048576) {
                return ( round( ($filesizeInByte/1048576), 2).' MiB' );
            } else if ($filesizeInByte >= 1024) {
                return ( round( ($filesizeInByte/1024), 2).' KiB' );
            } else {
                return $filesizeInByte.' B';
            }
        }
    }

    public function linkFilter($asset) {
        if ($asset) {
            return $this->asset_helper->getAbsoluteUrl($asset);
        }
        return $this->asset_helper->get404($asset);
    }

    /**
     * @deprecated use linkFilter now
     */
    public function pathFilter($asset) {
        return $this->linkFilter($asset);
    }

    /**
     * creates and returns thumbnails in given pixel size.
     * @param  [type] $asset  [description]
     * @param  [type] $height [description]
     * @param  [type] $width  [description]
     * @return [type]         [description]
     */
    public function thumbnailFilter($asset, $height, $width) {
        return $this->asset_helper->createThumbnail($asset, $height, $width);
    }

    public function isImageFilter($asset) {
        if (!$asset) {
            return false;
        }
        if ($asset->getMimetype() == 'image/png' || $asset->getMimetype() == 'image/jpeg') {
            return true;
        }
        return false;
    }

    public function getName() {
        return 'app_extension';
    }
}
