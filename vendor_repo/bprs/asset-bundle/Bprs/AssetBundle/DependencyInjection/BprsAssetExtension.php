<?php

namespace Bprs\AssetBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class BprsAssetExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container) {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');
        $container->setParameter('bprs_asset.adapters', $config['adapters']);
        $container->setParameter('bprs_asset.class', $config['class']);
        $container->setParameter('bprs_asset.worker_queue', $config['worker_queue']);
        $container->setParameter('bprs_asset.filesystem_map', $config['filesystem_map']);
        $container->setParameter('bprs_asset.404', $config['404']);
        $container->setParameter('bprs_asset.xsendfile', $config['xsendfile']);
        $container->setParameter('bprs_asset.thumb_filesystem', $config['thumb_filesystem']);
    }
}
