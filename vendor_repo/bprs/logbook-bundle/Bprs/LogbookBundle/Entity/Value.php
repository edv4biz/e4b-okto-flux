<?php

namespace Bprs\LogbookBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Value
 *
 * @ORM\Table(name="Entryvalue")
 * @ORM\Entity
 */
class Value
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="valuekey", type="string", length=20)
     */
    private $valueKey;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=200)
     */
    private $value;

    /**
     * @ORM\ManyToOne(targetEntity="Entry", inversedBy="values")
     * @ORM\JoinColumn(name="entry_id", referencedColumnName="id")
     */
    private $entry;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set valueKey;
     *
     * @param string $valueKey;
     * @return Value
     */
    public function setValueKey($valueKey)
    {
        $this->valueKey = $valueKey;

        return $this;
    }

    /**
     * Get valueKey;
     *
     * @return string
     */
    public function getValueKey()
    {
        return $this->valueKey;
    }

    /**
     * Set value;
     *
     * @param string $value;
     * @return Value
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value;
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set entry
     *
     * @param Entry $entry
     * @return Value
     */
    public function setEntry($entry = null)
    {
        $this->entry = $entry;

        return $this;
    }

    /**
     * Get entry
     *
     * @return \Bprs\LogbookBundle\Entity\BprsLogbook:Entry
     */
    public function getEntry()
    {
        return $this->entry;
    }
}
