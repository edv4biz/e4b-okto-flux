<?php

namespace Bprs\LogbookBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Bprs\LogbookBundle\Entity\Entry;

class WriteLogCommand extends ContainerAwareCommand
{
    public function __construct() {
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('bprs:logbook:write')
            ->setDescription('Add a logline')
            ->addArgument(
                'line',
                InputArgument::REQUIRED,
                'What do you want to log?'
            )->addOption(
                'type',
                't',
                InputOption::VALUE_REQUIRED,
                'Whats type do you want to log? (info|warning|error)',
                Entry::LOGTYPE_INFO
            )->addOption(
                'reference',
                'r',
                InputOption::VALUE_OPTIONAL,
                'Whats the reference you want to give? (uniqID)'
            )
            ->addArgument('args', InputArgument::IS_ARRAY, 'all log related args (name:bprs foo:bar bar:baz)');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->getContainer()->get('logger')->info('Writing Logbook entry');

        $arguments = [];
        foreach ($input->getArgument("args") as $arg) {
            $parts = explode(':', $arg);
            $arguments[$parts[0]] = $parts[1];
        }

        $this->getContainer()->get('bprs_logbook')->write(
            $input->getArgument("line"),
            $input->getOption("type"),
            $input->getOption("reference"),
            $arguments
        );

        $output->writeln("Finished writing Logline");
    }
}
