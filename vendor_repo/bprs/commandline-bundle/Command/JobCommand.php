<?php

namespace Bprs\CommandLineBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

class JobCommand extends ContainerAwareCommand
{
    private $jobservice;
    private $worker_queue;

    public function __construct($jobService, $worker_queue)
    {
        $this->jobservice = $jobService;
        $this->worker_queue = $worker_queue;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('bprs:commandline:add_job')
            ->setDescription('adds a given job in a given queue')
            ->addArgument('job', InputOption::VALUE_REQUIRED, 'name of job')
            ->addArgument('queue', InputOption::VALUE_REQUIRED, 'name of queue')
            ->addArgument('args', InputArgument::IS_ARRAY, 'all job related args (name:bprs input:a output:b)');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $job = $input->getArgument("job");
        $queue = $input->getArgument("queue", $this->worker_queue);
        $args = $input->getArgument("args");
        $arguments = array();
        foreach ($args as $arg) {
            $parts = explode(':', $arg);
            $arguments[$parts[0]] = $parts[1];
        }
        $token = $this->jobservice->addJob($job, $arguments);
        $output->writeln('Added job ['.$token.']');
    }
}
