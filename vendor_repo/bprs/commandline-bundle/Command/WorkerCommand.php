<?php

namespace Bprs\CommandLineBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Bprs\CommandLineBundle\Resque\Worker;

/**
 * Creates and starts a new worker. The logger, default queue and redis_backend
 * are in the config.yml
 */
class WorkerCommand extends ContainerAwareCommand
{
    public function __construct($logger, $worker_queue, $redis_backend = 'tcp://localhost:6379')
    {
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('bprs:commandline:worker')
            ->setDescription('Starts a worker for the given queue in foreground (great for debugging). If you want to start in in background, use start_worker.')
            ->addArgument(
                'queue',
                InputArgument::IS_ARRAY,
                'name of queue(s)'
            )
            ->addOption(
                'interval',
                'i',
                InputOption::VALUE_REQUIRED,
                'Interval in seconds to wait for between reserving jobs',
                5
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $resque = $this->getContainer()->get('bprs_jobservice')->getResque();
        $worker = new Worker($resque, $input->getArgument('queue'));
        $worker->work($input->getOption('interval'));
    }
}
