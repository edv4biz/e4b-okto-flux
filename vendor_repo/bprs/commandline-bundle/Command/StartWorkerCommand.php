<?php

namespace Bprs\CommandLineBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;

class StartWorkerCommand extends ContainerAwareCommand
{
    private $php;
    private $bin_dir;

    public function __construct($php, $bin_dir)
    {
        $this->php = $php;
        $this->bin_dir = $bin_dir;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('bprs:commandline:start_worker')
            ->setDescription('Starts a worker in background for given queue(s)')
            ->addArgument(
                'queue',
                InputArgument::IS_ARRAY,
                'name of queue(s)'
            )
            ->addOption(
                'interval',
                'i',
                InputOption::VALUE_REQUIRED,
                'Interval in seconds to wait for between reserving jobs',
                5
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->startWorker(
            $input->getArgument('queue'),
            $input->getOption('interval')
        );
        $output->writeln('Startet new worker for queue(s).');
    }

    public function startWorker($queue = false, $interval = 5)
    {
        $command = sprintf(
            '%s %s/console bprs:commandline:worker',
            $this->php,
            $this->getContainer()->get('kernel')->getRootDir().$this->bin_dir
        );

        if ($queue) {
            $command = sprintf(
                '%s %s --interval=%s',
                $command,
                implode(" ", $queue),
                $interval
            );
        } else {
            $command = sprintf('%s "*" --interval=%s', $command, $interval);
        }

        exec(sprintf('%s %s', $command, " > /dev/null 2>&1 &"));
    }
}
