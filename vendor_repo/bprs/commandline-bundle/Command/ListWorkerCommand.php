<?php

namespace Bprs\CommandLineBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ListWorkerCommand extends ContainerAwareCommand
{
    private $jobservice;

    public function __construct($jobService)
    {
        $this->jobservice = $jobService;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('bprs:commandline:list_worker')
            ->setDescription('Lists all workers on this machine');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $workerpids = $this->jobservice->listWorkers();
        print_r($workerpids);
    }
}
