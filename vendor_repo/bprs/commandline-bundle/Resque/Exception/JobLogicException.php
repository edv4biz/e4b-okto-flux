<?php

namespace Bprs\CommandLineBundle\Resque\Exception;

use Bprs\CommandLineBundle\Resque\ResqueException;

class JobLogicException extends ResqueException
{
}
