<?php

namespace Bprs\CommandLineBundle\Resque;

class Version
{
    const VERSION = '2.1.x';
}
