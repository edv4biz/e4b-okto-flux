<?php
namespace Bprs\CommandLineBundle\Model;

use Bprs\CommandLineBundle\Resque\Resque;
use Resque\Job;
use Predis\Client;
use Resque\Job\StatusFactory;

/**
 * Lets you start, stop and pause worker
 * lets you enlist and remove jobs
 */
class JobService
{
    private $logger;
    private $worker_queue;
    private $redis_backend;
    private $resque;
    private $workercommand;

    public function __construct($logger, $worker_queue, $workercommand, $redis_backend = 'tcp://localhost:6379')
    {
        $this->logger = $logger;
        $this->worker_queue = $worker_queue;
        $this->redis_backend = $redis_backend;
        $this->workercommand = $workercommand;
        $this->resque = $this->getResque($redis_backend);
    }

    public function getResque($redis_backend = false)
    {
        if (!$this->resque) {
            $this->resque = new Resque($this->createClient($redis_backend), [], $this->logger);
        }
        return $this->resque;
    }

    public function createClient($redis_backend = false)
    {
        if ($redis_backend) {
            $redis_backend = $this->redis_backend;
        }

        return new Client($redis_backend);
    }

    /**
     * starts a worker in given queues and interval
     * @param array $queues string array of queues or false for default queue.
     * @param integer $interval frequency for checking for new jobs
     */
    public function startWorker($queues = false, $interval = 5)
    {
        if ($queues = false) {
            $queues = [$this->worker_queue];
        }

        $this->workercommand->startWorker($queues, $interval);
    }

    /**
     * returns an array of all known workers. can include dead workers.
     */
    public function listWorkers()
    {
        return $this->resque->getWorkerIds();
    }

    public function listStatus()
    {
        return $this->resque->getStatusFactory();
    }

    /**
     * tries to stop all known workers with quit. (kill -QUIT)
     * @param boolean $force will use kill -KILL.
     */
    public function stopWorkers($force = false)
    {
        $workerIds = $this->listWorkers();
        foreach ($workerIds as $key => $value) {
            $pid = explode(':', $value);
            if ($force) {
                shell_exec(sprintf("kill -KILL %s", $pid[1]));
            } else {
                shell_exec(sprintf("kill -QUIT %s", $pid[1]));
            }
        }
    }

    /**
     * stops a single worker by (process) ID (kill -QUIT)
     * @param boolean $force will use kill -KILL
     * @return boolean true if workerpid is known
     */
    public function stopWorker($workerpid, $force = false)
    {
        if (in_array($workerpid, $this->listWorkers())) {
            if ($force) {
                shell_exec(sprintf("kill -KILL %s", $workerpid));
            } else {
                shell_exec(sprintf("kill -QUIT %s", $workerpid));
            }
            return true;
        }
        return false;
    }

    /**
     * Add a given job to a given queue.
     * @param BprsJob $job   "Your\Job\Namespace\Class"
     * @param array $args your payload arguments for the job
     * @param string $queue you want to enqueue this job in (otherwise default queue).
     * @param boolean $first if you want to enqueue this job as next job in line
     * @param boolean $monitor will create a status monitor info (created, updated, status, etc)
     * @return returns token with job id, false if redis could not be reached.
     */
    public function addJob($job, $args, $queue = false, $first = false, $monitor = false)
    {
        if (!$queue) {
            $queue = $this->worker_queue;
        }

        $id = $this->getResque()->enqueue($queue, $job, $args, $monitor, $first);

        return $id;
    }

    /**
     * removes all enqueued jobs in the given (or default) queue.
     */
    public function clearQueue($queue = false)
    {
        if (!$queue) {
            $queue = $this->worker_queue;
        }

        $this->getResque()->clearQueue($queue);
    }

    /**
     * Removes a job by its index in list (you can get the index with listJobs)
     *
     * @param integer index of job in redis list
     * @param  string $queue name of the queue (or default queue)
     * @return integer with number of removed jobs that matched. 0 if none found.
     */
    public function removeJob($index, $queue = false)
    {
        if (!$queue) {
            $queue = $this->worker_queue;
        }

        $removedJobs = $this->getResque()->remove($index, $queue);

        return $removedJobs;
    }

    /**
     * lists all jobs in a queue. Of course the are waiting to be executed.
     */
    public function listJobs($queue = false)
    {
        if (!$queue) {
            $queue = $this->worker_queue;
        }
        $size = $this->getResque()->size($queue);
        $list = $this->getResque()->list($queue, $size);

        $jobs = [];
        foreach ($list as $key => $entry) {
            $jobs[$key] = json_decode($entry, true);
        }

        return $jobs;
    }

    public function showJob($number=0, $queue = false)
    {
        // if (!$queue) {
        //     $queue = $this->worker_queue;
        // }
        //
        // \Resque::setBackend($this->redis_backend);
        // $list = \Resque::redis()->lrange('queue:' . $queue, $number, $number+1);
        // $jobs = array();
        // foreach ($list as $entry) {
        //     $jobs[] = json_decode($entry, true);
        // }
        //
        // return $jobs;
    }

    /**
     * Returns jobstatus of given job in given queue
     * @param  [string] $queue name of queue
     * @param  [string] $token   name of job
     * @return [Resque_Job_Status] the const Resque_Job_Status. false, if failed to fetch status
     */
    public function getJobStatus($token, $queue = false)
    {
        $factory = new StatusFactory($this->getResque());
        $status = $factory->forId($token);
        die(var_dump($status->getStatus()));
        // if (!$queue) {
        //     $queue = $this->worker_queue;
        // }
        // \Resque::setBackend($this->redis_backend);
        // $status = new \Resque_Job_Status($token);
        //
        // switch ($status->get()) {
        //     case \Resque_Job_Status::STATUS_WAITING:
        //         return sprintf('Job with ID [%s] is WAITING in QUEUE [%s]', $token, $queue);
        //     case \Resque_Job_Status::STATUS_RUNNING:
        //         return sprintf('Job with ID [%s] is RUNNING in QUEUE [%s]', $token, $queue);
        //     case \Resque_Job_Status::STATUS_FAILED:
        //         return sprintf('Job with ID [%s] FAILED in QUEUE [%s]. Check the worker.log on the correct machine for more information', $token, $queue);
        //     case \Resque_Job_Status::STATUS_COMPLETE:
        //         return sprintf('Job with ID [%s] COMPLETED in QUEUE [%s]', $token, $queue);
        //     default:
        //         return sprintf('Job not found, QUEUE [%s] or ID [%s] incorrect?', $queue, $token);
        // }
    }

    public function getAllJobStatuses($queue = false)
    {
        // if (!$queue) {
        //     $queue = $this->worker_queue;
        // }
        // \Resque::setBackend($this->redis_backend);
        // $tokens = \Resque::redis()->keys('*status*');
        // $statuss = [];
        // foreach ($tokens as $token) {
        //     $statuss[] = $this->getJobStatus($token, $queue);
        // }
        // return $statuss;
    }
}
