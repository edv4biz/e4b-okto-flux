<?php

namespace Bprs\CommandLineBundle\Model;

use Resque\AbstractJob;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Bprs\CommandLineBundle\Model\BprsContainerAwareJob;

class GreetJob extends BprsContainerAwareJob
{
    public function perform()
    {
        // with $this->getContainer(), you have access to all your services.
        $logger = $this->getContainer()->get('logger');
        $logger->info(sprintf("Greetings %s", $this->payload['args']['name']));

        // if you run the bprs:commandline:worker in the foreground, you'll see this message
        echo sprintf("Greetings %s", $this->payload['args']['name']);
    }
}
