<?php
namespace Bprs\CommandLineBundle\Model;

use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\Request;
use Bprs\CommandLineBundle\Resque\AbstractJob;

abstract class BprsContainerAwareJob extends AbstractJob {

    private static $kernel;

    public function getContainer() {
        if(self::$kernel != NULL) {
            if(self::$kernel->getContainer() == NULL){
                self::$kernel->boot();
            }
            return self::$kernel->getContainer();
        }

        self::$kernel = new \AppKernel('prod', false);
        self::$kernel->boot();
        return self::$kernel->getContainer();
    }

    public static function shutDown() {
        self::$kernel->shutdown();
    }

    public function getName() {
        return 'Container Aware Job';
    }
}
