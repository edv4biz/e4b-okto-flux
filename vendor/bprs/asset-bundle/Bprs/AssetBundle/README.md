# BPRSAssetBundle
Simple assets for your application. Upload Pictures, videos and more and link it with your entities

## Installation

in your composer.json file, add to "require"
```
composer require bprs/asset-bundle
```
OR
```json
"require": {
    "bprs/asset-bundle": "^2.0"
```

Activate the bundle in the AppKernel
```php
$bundles = array(
    ...
    new Bprs\AssetBundle\BprsAssetBundle()
    ...
);
```
And your Filesystem manager of choice
```php
$bundles = array(
    ...
    new Knp\Bundle\GaufretteBundle\KnpGaufretteBundle()
    ...
);
```
OR
```php
$bundles = array(
    ...
    new Oneup\FlysystemBundle\OneupFlysystemBundle()
    ...
);
```

## Configuration

The BPRSAssetBundle is on top of the OneupUploaderBundle. This allows for maximum flexibility.

### Gaufrette 

```json
# Example config
bprs_asset:
    class: "AppBundle\Entity\Asset"
    adapters:
        gallery:
            url: "http://localhost/MyProject/web/uploads"
            path: "/opt/local/apache2/htdocs/MyProject/web/uploads"
            
knp_gaufrette:
    adapters:
        gallery:
            local:
                directory: %kernel.root_dir%/../web/uploads
                create: true

    filesystems:
        gallery:
            adapter: gallery
            
oneup_uploader:
    mappings:
        gallery:
            frontend: blueimp
            storage:
                type: gaufrette
                filesystem: gaufrette.gallery_filesystem
                sync_buffer_size: 100M
```

### Flysystem

## Usage

### Relations

Assets can be standalone, or be in OneToOne, OneToMany or in a ManyToMany Relationship. 
To be that flexible, the Asset Entity is now a MappedSuperclass. Your Asset can now include any needed properties and your database remains clean.

The BaseAsset already defines several useful informations:
- createdAt (automatically set)
- updatedAt (automatically set and updated)
- filekey (the unique filename)
- adapter (the name of the filesystem adapter it is saved under)
- mimetype (for example image/jpeg, quicktime/video)
- filesize (in bytes)
- md5 (hash)
- name (original filename)

```php
<?php

// Example asset childclass

namespace Your\Name\Space;

use Doctrine\ORM\Mapping as ORM;
use Bprs\AssetBundle\Entity\Asset as BaseAsset;
/**
 * Extends the MappedSupperclass from the BprsAssetBundle
 *
 * @ORM\Entity
 * @ORM\Table()
 */
class Asset extends BaseAsset
{
    // Whatever your Asset needs
    //...
    //...
}
?>
```

### Forms

The BPRSAssetBundle includes a custom Formtype, a Datatransformer and even the needed JavaScript. 
Lets say, you have an entity that is linked to many assets:

```php
// form builder
public function buidlForm(FormBuilderInterface $builder, array $options) {
    $builder->add('assets', 'assets'); 
}
```
Or just one asset
```php
// form builder
public function buidlForm(FormBuilderInterface $builder, array $options) {
    $builder->add('asset', 'asset'); 
}
```

Next, the View:

```twig
    {{ form_start(form) }}    
        {% include "BprsAssetBundle::upload.html.twig" with {'form': form, 'adapter': 'gallery', 'hidePrevious': true} %}
    {{ form_end(form) }}
    
    {% block stylesheets %}
    {{ parent() }}
    <link href="{{ asset('bundles/bprsasset/css/jquery.fileupload.css') }}" rel="stylesheet" />
{% endblock %}

{% block javascripts %}
    {{ parent() }}
    {# if you don't use the BprsStylBundle, you'll need to include jquery yourself! #}
    {% include "BprsAssetBundle::upload.js.twig" %}    
{% endblock %}
```
### Thumbnails

The Bundle comes with an handy thumbnail filter for twig. All tou need to do is
```twig
# asset|thumb(height,width)
<img src="{{ asset|thumb(720,1280) }}" />
```
To create and display thumbnails on the fly.

### Filesize and Relative Paths

```twig
<a href="{{ asset|link }}">{{ asset.name|filesize(false) }}</a>
```

### Download and List

Activate the routing with

```yml
#routing.yml

bprs_asset:
    resource: .
    type: bprs_asset
    prefix:   /{_locale}
    requirements:
        _locale:  de|en
    defaults: { _locale: de }

```
You can secure downloading with security settings.

path "bprs_asset_list (page: page)" Lists all known Assets.  
path "bprs_asset_download (filekey: filekey)" allows downloads. (you'll probably need xsendfile for large files)
