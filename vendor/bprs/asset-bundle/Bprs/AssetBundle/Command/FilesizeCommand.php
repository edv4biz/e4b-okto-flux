<?php

namespace Bprs\AssetBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class FilesizeCommand extends ContainerAwareCommand {

    public function __construct() {
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('bprs:assetbundle:calc_filesize')
            ->setDescription('Generates missing filesize without a job');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $assets = $this->getContainer()->get('bprs_asset.repository')->findBy(['filesize' => 0]);
        $helper = $this->getContainer()->get('bprs.asset_helper');
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $output->writeln(sprintf("Found %s Assets without filesize", count($assets)));
        $output->writeln("Starting Calculations");
        foreach ($assets as $asset) {
            $url = $helper->getAbsoluteUrl($asset);
            $headers = array_change_key_case(get_headers($url, 1));
            $asset->setFilesize($headers['content-length']);
        }
        $output->writeln("Starting Batch Update");
        $batch = 0;
        foreach ($assets as $asset) {
            $em->persist($asset);
            $batch++;
            if ($batch > 10) {
                $em->flush();
                $em->clear();
                $batch = 0;
            }
        }
        $em->flush();
        $em->clear();

        $output->writeln("Updated all missing filesizes checksums.");

        $assets = $this->getContainer()->get('bprs_asset.repository')->findBy(['md5' => null]);
        if ($assets) {
            $output->writeln("These Assets are still missing a filesize");
            foreach ($assets as $asset) {
                $output->writeln($asset->getFilekey());
            }
        }
    }
}
