<?php

namespace Bprs\AssetBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class MoveFileCommand extends ContainerAwareCommand {

    public function __construct() {
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('bprs:assetbundle:move_file_job')
            ->setDescription('Adds a move file job')
            ->addArgument('filekey', InputArgument::REQUIRED, 'Asset filekey')
            ->addArgument('adapter', InputArgument::REQUIRED, 'The Adapter you want to move to')
            ->addArgument('queue', InputArgument::OPTIONAL, 'the queue you want to use');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $asset = $this->getContainer()->get('bprs.asset')->getAsset(
            $input->getArgument('filekey')
        );

        $queue = $input->getArgument('queue');
        if (!$queue) {
            $queue = false;
        }

        $this->getContainer()->get('bprs.asset_job')->addMoveAssetJob(
            $asset,
            $input->getArgument('adapter'),
            $input->getArgument('queue'),
            $queue
        );
    }
}
