<?php

namespace Bprs\AssetBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ThumbnailDelete extends ContainerAwareCommand {

    public function __construct() {
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('bprs:assetbundle:delete_thumb')
            ->setDescription('Deletes thumbnails of given asset (filekey)')
            ->addArgument('filekey', InputArgument::REQUIRED, 'filekey of asset you want to delete thumbnails of (e.g. 1234567890.jpg)');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $asset = $this->getContainer()->get('bprs.asset')->getAsset($input->getArgument('filekey'));
        if ($asset) {
            $output->writeln("Found Asset. Start Deleting Thumbnails");
            $this->getContainer()->get('bprs.asset_helper')->deleteThumbnails($asset);
            $output->writeln("Thumbnails deleted");
        } else {
            $output->writeln("Can't find Asset with filekey: ".$asset->getFilekey());
        }
    }
}
