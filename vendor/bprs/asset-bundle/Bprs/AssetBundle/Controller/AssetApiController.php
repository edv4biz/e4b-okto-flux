<?php

namespace Bprs\AssetBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Bprs\AssetBundle\Event\DeleteAssetEvent;
use Bprs\AssetBundle\BprsAssetEvent;
use Symfony\Component\EventDispatcher\ContainerAwareEventDispatcher;

/**
 * @Route("/")
 */
class AssetApiController extends Controller
{
    /**
     * @Route("bprs_asset/api/public/download", name="bprs_asset_download")
     */
    public function downloadAsset(Request $request)
    {
        $key = $request->query->get('filekey');
        $asset = $this->getDoctrine()->getManager()->getRepository($this->container->getParameter('bprs_asset.class'))->findOneBy(['filekey' => $key]);
        if ($asset) {
            if (!$this->get('bprs.asset_helper')->isRemote($asset)) {
                $response = new Response();
                $response->headers->set('Content-Disposition', sprintf('attachment; filename="%s"', $asset->getName()));
                $response->headers->set('Content-type', $asset->getMimetype());

                if ($this->container->getParameter('bprs_asset.xsendfile')) {
                    $response->headers->set('X-Sendfile', $this->get('bprs.asset_helper')->getPath($asset));
                    $response->sendHeaders();
                    return $response;
                }

                $filesystem = $this->get('bprs.asset_helper')->getFilesystem($asset->getAdapter());
                $file = $filesystem->get($asset->getFilekey());

                // Set headers
                $response->headers->set('Cache-Control', 'private');
                $response->headers->set('Content-length', $file->getSize());

                // // Send headers before outputting anything
                $response->sendHeaders();
                $response->setContent($file->read());

                return $response;
            } // $asset is not in a local filesystem. redirect to remote url
            return new RedirectResponse($this->get('bprs.asset_helper')->getAbsoluteUrl($asset));
        }
        return new Response("", Response::HTTP_BAD_REQUEST);
    }


    /**
     * @Route("bprs_asset/api/private/download", name="bprs_asset_api_download")
     */
    public function downloadApiAsset(Request $request)
    {
        $key = $request->query->get('filekey');
        $asset = $this->getDoctrine()->getManager()->getRepository($this->container->getParameter('bprs_asset.class'))->findOneBy(array('filekey' => $key));
        if ($asset) {
            if (!$this->get('bprs.asset_helper')->isRemote($asset)) {
                $response = new Response();
                $response->headers->set('Content-Disposition', sprintf('attachment; filename="%s"', $asset->getName()));
                $response->headers->set('Content-type', $asset->getMimetype());

                if ($this->container->getParameter('bprs_asset.xsendfile')) {
                    $response->headers->set('X-Sendfile', $this->get('bprs.asset_helper')->getPath($asset));
                    $response->sendHeaders();
                    return $response;
                }

                $filesystem = $this->get('bprs.asset_helper')->getFilesystem($asset->getAdapter());
                $file = $filesystem->get($asset->getFilekey());
                // Set headers
                $response->headers->set('Cache-Control', 'private');
                $response->headers->set('Content-type', $asset->getMimetype());
                $response->headers->set('Content-length', $file->getSize());

                // // Send headers before outputting anything
                $response->sendHeaders();
                $response->setContent($file->read());

                return $response;
            } // $asset is not in a local filesystem. redirect to remote url
            return new RedirectResponse($this->get('bprs.asset_helper')->getAbsoluteUrl($asset));
        }
        return new Response("", Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Route("bprs_asset/api/public/show", name="bprs_asset_api_show")
     */
    public function showAssetAction(Request $request)
    {
        $key = $request->query->get('filekey');
        $asset = $this->getDoctrine()->getManager()->getRepository($this->container->getParameter('bprs_asset.class'))->findOneBy(array('filekey' => $key));
        if ($asset) {
            $asset_helper = $this->get('bprs.asset_helper');
            if ($request->query->get('width') && $request->query->get('height') && $asset_helper->isImage($asset)) {
                $url = $asset_helper->getThumbnail($asset, $request->query->get('height'), $request->query->get('width'));
                return new RedirectResponse($url);
            }
            return new RedirectResponse($this->get('bprs.asset_helper')->getAbsoluteUrl($asset));
        }
        return new Response("", Response::HTTP_NOT_FOUND);
    }

    /**
     * @Route("/bprs_asset/api/asset.{_format}", defaults={"_format": "json"}, requirements={"_format": "json|xml"}, name="bprs_asset_api_metadata")
     * @Security("is_granted('ROLE_BPRS_ASSET_READ')")
     * @Method("GET")
     * @Template()
     */
    public function showAssetMetadataAction(Request $request, $_format)
    {
        $filekey = $request->query->get('filekey');
        if ($filekey) {
            $asset = $this->get('bprs.asset')->getAsset($filekey);
            return ['asset' => $asset];
        }
        return new Response("", Response::HTTP_BAD_REQUEST);
    }
}
