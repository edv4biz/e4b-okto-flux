<?php

namespace Bprs\AssetBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Bprs\AssetBundle\Event\DeleteAssetEvent;
use Bprs\AssetBundle\BprsAssetEvent;
use Symfony\Component\EventDispatcher\ContainerAwareEventDispatcher;

/**
 * @Route("/bprs_asset")
 * @Security("is_granted('ROLE_BPRS_ASSET_READ')")
 */
class AssetController extends Controller
{
    /**
     * @Route("/index", name="bprs_asset_list")
     * @Template()
     */
    public function listAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $asset_class = $this->container->getParameter('bprs_asset.class');
        $query = $em->getRepository($asset_class)->findForAdapter(
            $asset_class,
            $request->query->get('adapter', false),
            0,
            true
        );

        $paginator = $this->get('knp_paginator');
        $assets = $paginator->paginate(
            $query,
            $request->query->get('page', 1),
            $request->query->get('results', 10)
        );

        return ['assets' => $assets, 'adapters' => $this->getParameter('bprs_asset.adapters'), 'filtered' => $request->query->get('adapter', false)];
    }

    /**
     * @Route("/show/{filekey}", name="bprs_asset_show")
     * @Template()
     */
    public function showAction($filekey)
    {
        $asset = $this->get('bprs.asset')->getAsset($filekey);
        return ['asset' => $asset, 'adapters' => $this->getParameter('bprs_asset.adapters')];
    }

    /**
     * @Route("/modal", name="bprs_asset_modal_list")
     * @Template()
     */
    public function modalListAction(Request $request)
    {
        return ['page' => $request->query->get('page', 1), 'adapter' => $request->query->get('adapter', null)];
    }

    /**
     * @Route("/ajax", name="bprs_asset_ajax_list")
     * @Template()
     */
    public function ajaxModalListAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $asset_class = $this->container->getParameter('bprs_asset.class');
        $query = $em->getRepository($asset_class)
            ->findAssetsFromAdapter($asset_class, 0, $request->query->get('adapter', null), true);
        $paginator = $this->get('knp_paginator');
        $assets = $paginator->paginate($query, $request->query->get('page', 1), 5);
        $assets->setUsedRoute('bprs_asset_ajax_list');

        return ['assets' => $assets];
    }

    /**
     * @Route("/delete_thumbnails/{filekey}", name="bprs_asset_delete_thumbs")
     */
    public function deleteThumbnails($filekey)
    {
        $asset_service = $this->get('bprs.asset');
        $asset = $this->get('bprs.asset')->getAsset($filekey);
        $asset_service->getHelper()->deleteThumbnails($asset);
        $this->get('session')->getFlashBag()->add('info', 'bprs_asset.info_delete_thumbnails');
        return $this->redirect($this->generateUrl('bprs_asset_show', ['filekey' => $filekey]));
    }

    /**
     * @Route("/move", name="bprs_asset_move")
     * @Security("is_granted('ROLE_BPRS_ASSET_WRITE')")
     */
    public function moveAsset(Request $request)
    {
        $filekey = $request->query->get('filekey', false);
        $adapter = $request->query->get('adapter', false);
        if ($filekey && $adapter) {
            $asset_service = $this->get('bprs.asset');
            $asset = $asset_service->getAsset($filekey);
            if ($asset) {
                $this->get('bprs.asset_job')->addMoveAssetJob($asset, $adapter);
                return new Response("", Response::HTTP_OK);
            }
        }
        return new Response(Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Route("/md5", name="bprs_asset_md5")
     * @Security("is_granted('ROLE_BPRS_ASSET_WRITE')")
     */
    public function md5Asset(Request $request)
    {
        $filekey = $request->query->get('filekey', false);
        if ($filekey) {
            $asset_service = $this->get('bprs.asset');
            $asset = $asset_service->getAsset($filekey);
            if ($asset) {
                $this->get('bprs.asset_job')->addMD5Job($asset);
                return new Response("", Response::HTTP_OK);
            }
        }
        return new Response(Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Route("/filesize", name="bprs_asset_filesize")
     * @Security("is_granted('ROLE_BPRS_ASSET_WRITE')")
     */
    public function filesizeAsset(Request $request)
    {
        $filekey = $request->query->get('filekey', false);
        if ($filekey) {
            $asset_service = $this->get('bprs.asset');
            $asset = $asset_service->getAsset($filekey);
            if ($asset) {
                $this->get('bprs.asset_job')->addFilesizeJob($asset);
                return new Response("", Response::HTTP_OK);
            }
        }
        return new Response(Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Route("/crop", name="bprs_asset_crop")
     * @Security("is_granted('ROLE_BPRS_ASSET_WRITE')")
     */
    public function cropAsset(Request $request)
    {
        $width = $request->request->get('width');
        $height = $request->request->get('height');
        $x = $request->request->get('x');
        $y = $request->request->get('y');
        $filekey = $request->request->get('filekey');

        if ($width && $height && $filekey ) {
            $asset = $this->getDoctrine()->getManager()->getRepository(
                $this->container->getParameter('bprs_asset.class'))->findOneBy(array('filekey' => $filekey)
            );
            $this->get('bprs.asset_helper')->cropAsset($asset, $x, $y, $width, $height);
            return new Response("", Response::HTTP_OK);
        }

        return new Response($width.' '.$height.' '.$x.' '.$y, Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Route("/delete", name="bprs_asset_delete")
     * @Security("is_granted('ROLE_BPRS_ASSET_DELETE')")
     */
    public function deleteAsset(Request $request)
    {
        $filekey = $request->query->get('filekey');
        if ($filekey) {
            $asset_service = $this->get('bprs.asset');
            $asset = $asset_service->getAsset($filekey);
            $this->get('bprs.asset')->deleteAsset($asset);

            return new Response("", Response::HTTP_OK);
        }
        return new Response("", Response::HTTP_BAD_REQUEST);
    }
}
?>
