<?php

namespace Bprs\AssetBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class CreateAssetEvent extends Event
{
    protected $asset;
    protected $original_event;

    public function __construct($asset, $original_event) {
        $this->asset = $asset;
        $this->original_event = $original_event;
    }

    public function getAsset() {
        return $this->asset;
    }

    public function getOriginalEvent()
    {
        return $this->original_event;
    }
}
