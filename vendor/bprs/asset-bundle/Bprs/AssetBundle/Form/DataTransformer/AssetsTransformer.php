<?php
namespace Bprs\AssetBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Bprs\AssetBundle\Entity\Asset;

class AssetsTransformer implements DataTransformerInterface
{
    private $repository;

    public function __construct(\Doctrine\ORM\EntityRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Transforms objects (assets) to strings (keys).
     *
     * @param  Assets|null $assets
     * @return string
     */
    public function transform($assets)
    {
        if (null === $assets) {
            return null;
        }

        $filekeys = [];
        foreach ($assets as $asset) {
            $filekeys[] = $asset->getFilekey();
        }

        return $filekeys;
    }

    /**
     * Transforms strings (keys) to objects (assets).
     *
     * @param  array $filekeys
     *
     * @return Assets|null
     *
     * @throws TransformationFailedException if object (asset) is not found.
     */
    public function reverseTransform($filekeys)
    {
        if (!$filekeys) {
            return null;
        }
        $assets = [];
        foreach ($filekeys as $filekey) {
            $asset = $this->repository->findOneBy(['filekey' => $filekey]);

            if (null === $asset) {
                throw new TransformationFailedException(sprintf(
                    'An asset with key "%s" does not exist!',
                    $filekey
                ));
            }
            $assets[] = $asset;
        }

        return $assets;
    }
}
