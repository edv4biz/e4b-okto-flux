<?php
namespace Bprs\AssetBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Bprs\AssetBundle\Entity\Asset;

class AssetTransformer implements DataTransformerInterface
{

    private $repository;

    /**
     * @param ObjectManager $om
     */
    public function __construct(\Doctrine\ORM\EntityRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Transforms object (assets) to a string (key).
     *
     * @param  Asset|null $asset
     * @return string
     */
    public function transform($asset)
    {
        if (null === $asset) {
            return "";
        }

        return $asset->getFilekey();
    }

    /**
     * Transforms a string (key) to an object (asset).
     *
     * @param  string $filekey
     *
     * @return Asset|null
     *
     * @throws TransformationFailedException if object (asset) is not found.
     */
    public function reverseTransform($filekey)
    {
        if (!$filekey) {
            return null;
        }

        $asset = $this->repository->findOneBy(array('filekey' => $filekey));

        if (null === $asset) {
            throw new TransformationFailedException(sprintf(
                'An asset with key "%s" does not exist!',
                $filekey
            ));
        }

        return $asset;
    }
}
