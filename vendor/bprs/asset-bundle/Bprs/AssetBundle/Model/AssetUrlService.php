<?php

namespace Bprs\AssetBundle\Model;

use Bprs\AppLinkBundle\BprsAppLinkUrlInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class AssetUrlService implements BprsAppLinkUrlInterface
{
    private $router;

    public function __construct($router)
    {
        $this->router = $router;
    }

    public function getUrls()
    {
        $route_names = ['bprs_asset_api_download', 'bprs_asset_download', 'bprs_asset_api_show', 'bprs_asset_api_metadata'];
        $urls = [];
        foreach ($route_names as $route_name) {
            $urls[$route_name] = $this->router->generate($route_name, [], UrlGeneratorInterface::ABSOLUTE_URL);
        }
        return $urls;
    }
}

?>
