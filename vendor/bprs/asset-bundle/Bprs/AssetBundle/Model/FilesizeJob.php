<?php

namespace Bprs\AssetBundle\Model;

use Bprs\CommandLineBundle\Model\BprsContainerAwareJob as BaseJob;

class FilesizeJob extends BaseJob {

    /**
     * gets content-length of http request and saves to filekey
     */
    public function perform() {
        $asset = $this->getContainer()->get('bprs.asset')->getAsset($this->args['filekey']);
        if ($asset) {
            $url = $this->getContainer()->get('bprs.asset_helper')->getAbsoluteUrl($asset);
            if ($url) {
                $headers = array_change_key_case(get_headers($url, 1));
                $asset->setFilesize($headers['content-length']);
                $em = $this->getContainer()->get('doctrine.orm.entity_manager');
                $em->persist($asset);
                $em->flush();
            }
        }
    }

    public function getName() {
        return 'Filesize Job';
    }
}
?>
