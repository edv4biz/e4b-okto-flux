<?php

namespace Bprs\AssetBundle\Model;

use Bprs\CommandLineBundle\Model\BprsContainerAwareJob as BaseJob;

class MD5Job extends BaseJob {

    /**
     * gets file like using gaufrette
     * makes md5 hash and saves it to the database
     */
    public function perform() {
        $asset = $this->getContainer()->get('bprs.asset')->getAsset($this->args['filekey']);
        if ($asset) {
            $url = $this->getContainer()->get('bprs.asset_helper')->getAbsoluteUrl($asset);
            if ($url) {
                $md5 = md5_file($this->getContainer()->get('bprs.asset_helper')->getAbsoluteUrl($asset));
                $asset->setMd5($md5);
                $em = $this->getContainer()->get('doctrine.orm.entity_manager');
                $em->persist($asset);
                $em->flush();
            }
        }
    }

    public function getName() {
        return 'MD5 Job';
    }
}
?>
