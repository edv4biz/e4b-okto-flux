<?php

namespace Bprs\CommandLineBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class StopWorkerCommand extends ContainerAwareCommand
{
    private $jobservice;

    public function __construct($jobService)
    {
        $this->jobservice = $jobService;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('bprs:commandline:stop_worker')
            ->addOption('force', false, InputOption::VALUE_NONE, 'force quit (kill -9)')
            ->setDescription('Stops all workers on this machine');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->jobservice->stopWorkers($input->getOption('force'));
    }
}
