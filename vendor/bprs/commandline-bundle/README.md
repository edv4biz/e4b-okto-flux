# BPRSCommandlineBundle

## Special Thanks
This bundle would not be possible if not for the great work of many developers out there. Especially The people behind the php-resque library (https://github.com/chrisboulton/php-resque) and the people from the vend/resque bundle (https://github.com/vend/php-resque)!


## About
This bundle allows symfony projects to execute long running tasks (jobs) in the background (for example video encoding, transferring big files from and to cloudservices, etc). Jobs are container aware, log information with monolog and workers can be started (and stopped) from the web or commandline.

This library needs a running redis database and connection. 

# Installation

```
composer require bprs/commandline-bundle
```

```php
$bundles = array(
    ...
    new Bprs\CommandLineBundle\BprsCommandLineBundle(),
    ...
```
You'll need redis and the redis php extension.

# Configuration
```yml
# app/config/config.yml

bprs_command_line:
    php_path: #path to the php that should be used to launch a worker. default: php
    worker_queue: #the default queue for jobs and worker to use. should be unique. default: *
    redis_backend: #the connection url. default: tcp://localhost:6379
```

# Usage

## Start redis
You'll need a running redis database. 

```
app/console bprs:commandline:start_worker QUEUENAME (--interval=5)
#starts a worker listening to the given queuename in given interval
#you can follow the log in apps/log/dev.log or prod.log

app/console bprs:commandline:stop_worker (--force)
#stops all worker after they finish the current job (graceful stop)
# --force will use kill -9

app/console bprs:commandline:add_job JOBNAME QUEUENAME argument1:arg, argument2:arg, ... argumentN:arg
#schedules a given job in a given queue with an array of given arguments
#example
app/console bprs:commandline:add_job Bprs\\CommandlineBundle\\Model\\GreetJob default name:BPRS

app/console bprs:commandline:list_jobs --queue=QUEUE
#lists all jobs in given queue. You can use the indexnumber and stop_job to remove a job from the queue

app/console bprs:commandline:list_worker
# lists all known worker. Can contain ungraceful stopped worker as well.
```
# Advanced usage
Of course, you can manipulate all the workers with POSIX reliable signals.
```
app/console bprs:commandline:list_worker
```
lists all workers on this machine. Also lists the process id. (pid)
You can now manipulate those pids
```
#stops a specific worker (gracefull stop)
kill -QUIT pid

#stops a specific worker. (ungracefull stop)
kill -9 pid

#see php-resque worker
pcntl_signal(SIGUSR1, array($this, 'killChild'));
pcntl_signal(SIGUSR2, array($this, 'pauseProcessing'));
pcntl_signal(SIGCONT, array($this, 'unPauseProcessing'));
pcntl_signal(SIGPIPE, array($this, 'reestablishRedisConnection'));
```

# Write your own Job
## normal job
Write your class. You can access all arguments given with the add_job command with the associative array "args"

```php
namespace Your\Cool\Namespace;

use Bprs\CommandlineBundle\Resque\AbstractJob;


class YourJob extends AbstractJob {
    public function perform()
    {
        echo sprintf("your task to perform");
        echo sprintf("your argument:%s", $this->args["yourKey"]);
    }
}
```
To get this up and running, do
```
# start an worker for the queue defaultQueue
app/console bprs:commandline:start_worker defaultQueue

# add your job with your arguments to the defaultQueue
app/console bprs:commandline:add_job Your\\Cool\\Namespace\\YourJob defaultQueue yourKey:yourArgument
```
Or use the service
```php

    $this->get('bprs_jobservice')->addJob(
        "Your\\Cool\\Namespace\\YourJob", //your job class
        ["your" => "arguments"],          //your payload arguments
        $onTopOfTheQueue,                 //if you want to enqueue as next job (skip list)
        $monitorMe                        //if you want detailed information for this job (more soon)
    );
```

If everything worked as expected, you should find in the log (app/logs/dev.log or prod.log)
```
...
[2017-04-12 15:37:50] app.NOTICE: Registered signals [] []
[2017-04-12 15:37:50] app.DEBUG: Registering worker yourmachine:5191:yourqueue [] []
[2017-04-12 15:37:50] app.DEBUG: Attempting to reserve job from YOURQUEUE {"queues":"yourqueues"} []
...
```

## container aware job
You can also extend ``` Bprs\CommandLineBundle\Model\BprsContainerAwareJob ``` and get the service container with 

```php
$this->getContainer()
```
in your job.
