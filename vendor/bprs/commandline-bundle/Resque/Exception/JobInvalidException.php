<?php

namespace Bprs\CommandLineBundle\Resque\Exception;

use Bprs\CommandLineBundle\Resque\ResqueException;

/**
 * Generally means the job class itself is invalid. Usually, because it does
 * not implement JobInterface
 */
class JobInvalidException extends ResqueException
{
}
