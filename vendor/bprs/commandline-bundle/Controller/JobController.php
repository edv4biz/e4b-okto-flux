<?php

namespace Bprs\CommandLineBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/bprs_commandline")
 * @Security("is_granted('ROLE_BPRS_COMMANDLINE_BACKEND')")
 */
class JobController extends Controller
{
    /**
     * @Route("/list_queue", name="list_jobs")
     * @Template()
     */
    public function listQueueAction(Request $request)
    {
        return ['queue' => $this->get('bprs_jobservice')->listJobs($request->query->get('queue', false))];
    }

    /**
     * @Route("/list_worker", name="bprs_commandline_list_worker")
     * @Template()
     */
    public function listWorkerAction(Request $request)
    {
        return ['workers' => $this->get('bprs_jobservice')->listWorkers()];
    }
}
