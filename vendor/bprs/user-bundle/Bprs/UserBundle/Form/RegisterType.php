<?php

namespace Bprs\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;

class RegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'username',
                TextType::class,
                [
                    'label' => 'bprs_user.user.name',
                    'attr' => ['placeholder' => 'bprs_user.user.name_placeholder']
                ]
            )
            ->add(
                'email',
                EmailType::class,
                [
                    'label' => 'bprs_user.user.email',
                    'attr' => ['placeholder' => 'bprs_user.user.email_placeholder']
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
                'data_class' => 'Bprs\UserBundle\Entity\User'
            ]);
    }

    public function getBlockPrefix()
    {
        return 'BprsUserBundle_RegisterType';
    }
}
