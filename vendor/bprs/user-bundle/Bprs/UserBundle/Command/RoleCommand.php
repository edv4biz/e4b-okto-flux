<?php

namespace Bprs\UserBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Bprs\UserBundle\Entity\User;
use Bprs\UserBundle\Entity\dbRole;

class RoleCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('bprs:userbundle:add_role')
            ->setDescription('Adds Role to an user')
            ->addOption('user', '-1', InputOption::VALUE_REQUIRED, 'name of the account')
            ->addOption('role', '-2', InputOption::VALUE_REQUIRED, 'Role you want to add');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $userService = $this->getContainer()->get('bprs_user.user');
        $user = $userService->getUser($input->getOption('user'));
        if ($user) {
            if (!in_array($input->getOption('role'), $user->getRoles())) {
                $this->getContainer()->get('bprs_user.user')->addRole(
                    $user,
                    $input->getOption('role')
                );
            }
            $output->writeln(sprintf('User [%s] now upgraded', $user->getUsername()));
        } else {
            $output->writeln(sprintf('Cant find User [%s]', $input->getOption('user')));
        }
    }
}
