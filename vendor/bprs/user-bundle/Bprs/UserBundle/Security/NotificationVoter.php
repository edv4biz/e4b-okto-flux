<?php

namespace Bprs\UserBundle\Security;

use Bprs\UserBundle\Entity\User;
use Bprs\UserBundle\Entity\Notification;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class NotificationVoter extends Voter
{
    const FOLLOW = 'user_follow';

    protected function supports($attribute, $subject)
    {
        // if the attribute isn't one we support, return false
        if (!in_array($attribute, [self::FOLLOW])) {
            return false;
        }

        // only vote on Post objects inside this voter
        if (!$subject instanceof Notification) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }

        switch($attribute) {
            case self::FOLLOW:
                return $this->canFollow($subject, $user);
        }

        throw new \LogicException('This code should not be reached!');
    }

    // allow user to post in the backend
    private function canFollow($notification, $user)
    {
        return $notification->getUser() === $user;
    }
}
