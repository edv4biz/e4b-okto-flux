<?php

namespace Bprs\UserBundle\Entity;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;

class UserRepository extends EntityRepository implements UserLoaderInterface
{
    public function loadUserByUsername($name)
    {
        $q = $this
            ->createQueryBuilder('u')
            ->where('u.username = :username OR u.email = :email')
            ->setParameter('username', $name)
            ->setParameter('email', $name)
            ->getQuery();

            try {
                $user = $q->getSingleResult();
            } catch (NoResultException $e) {
                throw new UsernameNotFoundException('bprs_user.message.bad_credentials', 0, $e);
            }

        return $user;
    }

    public function refreshUser(UserInterface $user)
    {
        $class = get_class($user);
        if (!$this->supportsClass($class)) {
            throw new UnsupportedUserException(sprintf('unsupported Class! (%s), did you forget to subclass BprsUserBundle:User?', $class));
        }

        return $this->find($user->getId());
    }

    public function supportsClass($class)
    {
        return $this->getEntityName() == $class || is_subclass_of($class, $this->getEntityName());
    }

    public function getUsers($userclass, $query_only = false)
    {
        $query = $this->getEntityManager()->createQuery(
            sprintf('SELECT u FROM %s u', $userclass)
        );

        if ($query_only) {
            return $query;
        }
        return $query->getResult();
    }
}
