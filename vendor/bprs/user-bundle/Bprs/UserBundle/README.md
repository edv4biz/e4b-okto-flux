# BPRSUserBundle
Simple, extendable Symfony Userbundle with RESTful API to share users between applications

## installation
in your composer.json file, add to "require"
```json
"require": {
    "bprs/user-bundle": "dev-master" 
}
```
(for stable versions, check out the latest release Tag!)

in your routing.yml, add 
```yaml
bprs_user:
    resource: .
    type: bprs_user
```

your security.yml needs to know how to use Bprs Users. Here is a simple example:
```yaml
security:
    role_hierarchy:
        ROLE_ADMIN: ROLE_USER

    encoders:
        Bprs\UserBUndle\Entity\User:
            algorithm: bcrypt
            cost: 12

    providers:
        database:
            entity: 
                class: YouYourBundle:YourUser
                property: username

    firewalls:
        main:
            pattern:    /
            http_basic: ~
            form_login:
                login_path: bprs_user_login
                check_path: bprs_user_login_check
                default_target_path: intake_backend
                csrf_provider: form.csrf_provider
                default_target_path: /de
            logout: 
                path: bprs_user_logout
                target: /de
            anonymous: ~

    access_control:
        - { path: /backend, roles: ROLE_USER }
        - { path: /backend/admin, roles: ROLE_ADMIN}
```
finally, in your config.yml, add to doctrine:
```yaml
doctrine:
    orm:
        resolve_target_entities:
            Bprs\UserBundle\Entity\BprsUserInterface: Your\YourBundle\Entity\YourUser
            
bprs_user:
    class: "YourBundle\Entity\YourUser"
    mail:
        commandline_host:   www.yourhost.com
        adress:             yourbot@example.com
        name:               yourbot
    permissions:
        - ROLE_BPRS_USER_BACKEND
        - ROLE_ADMIN
        - ROLE_USER
        # and whatever roles your application uses and a new created admin should have
    user_defaults:
        - ROLE_USER
        # and whatever roles a new created user should have
```

## Usage
### Extend the BprsUser Entity
The BprsUser Entity is a MappedSuperclass.

For example:
Create your desired User Entity like this:

```php
namespace You/YourBundle/Entity;

use Bprs/UserBundle/Entity/User as BprsUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * MyUser
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class MyUser extends BprsUser
{
    /**
     * @var string
     * @ORM\Column(name="foo", type="string", length=3)
     */
    private $foo;
    
    /** and so on **/
}
```
And thats it. Doctrine will handle the rest and merge both entities into one table.

### Create the first Admin user

```
# First (only 'needed' after a new installation or configuarion)
./app/console bprs:userbundle:warmup

./app/console bprs:userbundle:create_admin -1 yourname -2 your@email.com
```
The warmup commad adds all roles declared in the config 'permissions' to the database.  
The create admin account will add a admin user with all permissions automatically added. Don't use that to create normal users if you don't want them to be able to access all parts of your application!

### ROLES FOR THIS BUNDLE

ROLE_BPRS_USER_BACKEND -> Access to the UserController (CRUD)
