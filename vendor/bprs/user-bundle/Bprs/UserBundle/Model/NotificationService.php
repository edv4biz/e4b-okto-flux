<?php

namespace Bprs\UserBundle\Model;

use Bprs\UserBundle\Entity\Notification;
use Bprs\UserBundle\Entity\NotificationValue;

class NotificationService {

    private $em;
    private $mailer;
    private $translator;

    public function __construct($em, $mailer, $translator)
    {
        $this->em = $em;
        $this->mailer = $mailer;
        $this->translator = $translator;
    }

    public function addNewNotification($user, $url, $message, $values = [], $mailtemplate = false, $subject = "", $mailtemplatevalues = [])
    {
        $notification = new Notification($message);
        foreach ($values as $key => $value) {
            $value = new NotificationValue($key, $value);
            $notification->addValue($value);
        }
        $link = new NotificationValue('url', $url);
        $notification->addValue($link);

        $user->addNotification($notification);

        $this->em->persist($user);
        $this->em->persist($notification);
        $this->em->flush();

        if ($mailtemplate) {
            $this->mailer->sendmail($user->getEmail(), $mailtemplate, $mailtemplatevalues, $this->translator->trans($subject, $values));
        }
    }

    public function markAsRead($notification)
    {
        $notification->setRead(true);
        $this->em->persist($notification);
        $this->em->flush();
    }
}


?>
