<?php

namespace Bprs\LogbookBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/bprs_logbook")
 */
class DefaultController extends Controller
{
    /**
     * @Route(
     *  "/pager/{reference}/{page}.{_format}",
     *  name="bprs_logbook_by_reference",
     *  defaults={"page": 1, "_format": "html"},
     *  requirements={"page": "\d+"}
     *  )
     *
     * @Method("GET")
     * @Template()
     */
    public function pagerAction(Request $request, $reference, $page = 1)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em
            ->getRepository('BprsLogbookBundle:Entry')
            ->findByReference($reference, true);
        $paginator = $this->get('knp_paginator');
        $entries = $paginator->paginate($query, $page, $request->query->get('results', 5));
        $entries->setUsedRoute('bprs_logbook_by_reference', ['reference' => $reference]);
        $entries->setParam('reference', $reference);
        return array('entries' => $entries);
    }
}
