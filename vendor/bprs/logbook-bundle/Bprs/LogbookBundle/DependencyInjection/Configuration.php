<?php

namespace Bprs\LogbookBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Builder;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
	    $treeBuilder = new TreeBuilder('bprs_logbook');
	    if (\method_exists($treeBuilder, 'getRootNode')) {
		    $rootNode = $treeBuilder->getRootNode();
	    } else {
		    $rootNode = $treeBuilder->root('bprs_logbook');
	    }

        return $treeBuilder;
    }
}
