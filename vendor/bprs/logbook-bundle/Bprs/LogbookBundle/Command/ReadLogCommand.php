<?php

namespace Bprs\LogbookBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\Table;

use Bprs\LogbookBundle\Entity\Entry;

class ReadLogCommand extends ContainerAwareCommand
{
    public function __construct() {
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('bprs:logbook:read')
            ->setDescription('read loglines with reference in timeframe')
            ->addOption(
                'reference',
                'r',
                InputOption::VALUE_REQUIRED,
                'Whats the reference of the log?'
            )->addOption(
                'from',
                'f',
                InputOption::VALUE_REQUIRED,
                'From when do you want your logs? (datetime)',
                '-2 days'
            )->addOption(
                'to',
                't',
                InputOption::VALUE_REQUIRED,
                'Until when do you want your logs? (datetime)',
                'now'
            )
            ->addArgument('args', InputArgument::IS_ARRAY, 'all log related args (name:bprs foo:bar bar:baz)');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(sprintf(
            'Retrieving Entries for [%s] [%s] [%s]',
            $input->getOption('reference'),
            $input->getOption('from', '-2 days'),
            $input->getOption('to', 'now')
        ));
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $entries = $em->getRepository('BprsLogbookBundle:Entry')->findByReferenceInTime(
            $input->getOption('reference'),
            new \Datetime($input->getOption('from')),
            new \Datetime($input->getOption('to'))
        );

        $rows = [];
        foreach ($entries as $entry) {
            $rows[] = [$entry->getType(), $entry->getReference(), $entry, $entry->getCreatedAt()->format('Y.m.d H:i:s')];
        }

        $table = new Table($output);
        $table
            ->setHeaders(['type', 'reference','line', 'createdAt'])
            ->setRows($rows);

        $table->render();
    }
}
