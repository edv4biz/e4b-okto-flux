<?php
namespace Bprs\StyleBundle\Twig;

/**
 * Code shamelessly stolen from LiipUrlAutoConverterBundle.
 * https://github.com/liip/LiipUrlAutoConverterBundle/blob/master/Extension/UrlAutoConverterTwigExtension.php
 * because the bundle is not compatible with current dependencies
 */
class AutoUrlExtension extends \Twig_Extension
{
    protected $linkClass;
    protected $target;

    public function getName() {
        return 'bprs_autourl';
    }

    public function __construct($linkClass, $target) {
        $this->linkClass = $linkClass;
        $this->target = $target;
    }

    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter(
                'converturls',
                [$this, 'autoConvertUrls'],
                ['pre_escape' => 'html', 'is_safe' => ['html']]
            )
        ];
    }

    /**
     * method that finds different occurrences of urls or email addresses in a string.
     *
     * @param string $string input string
     *
     * @return string with replaced links
     */
    public function autoConvertUrls($string)
    {
        $pattern = '/(href="|src=")?([-a-zA-Zа-яёА-ЯЁ0-9@:%_\+.~#?&\*\/\/=]{2,256}\.[a-zа-яё]{2,4}\b(\/?[-\p{L}0-9@:%_\+.~#?&\*\/\/=\(\),;]*)?)/u';
        $stringFiltered = preg_replace_callback($pattern, [$this, 'callbackReplace'], $string);
        return $stringFiltered;
    }
    public function callbackReplace($matches)
    {
        if ($matches[1] !== '') {
            return $matches[0]; // don't modify existing <a href="">links</a> and <img src="">
        }
        $url = $matches[2];
        $urlWithPrefix = $matches[2];
        if (strpos($url, '@') !== false) {
            $urlWithPrefix = 'mailto:'.$url;
        } elseif (strpos($url, 'https://') === 0) {
            $urlWithPrefix = $url;
        } elseif (strpos($url, 'http://') !== 0) {
            $urlWithPrefix = 'http://'.$url;
        }

        // ignore tailing special characters
        // TODO: likely this could be skipped entirely with some more tweakes to the regular expression
        if (preg_match("/^(.*)(\.|\,|\?)$/", $urlWithPrefix, $matches)) {
            $urlWithPrefix = $matches[1];
            $url = substr($url, 0, -1);
            $punctuation = $matches[2];
        } else {
            $punctuation = '';
        }
        return '<a href="'.$urlWithPrefix.'" class="'.$this->linkClass.'" target="'.$this->target.'">'.$url.'</a>'.$punctuation;
    }
}
