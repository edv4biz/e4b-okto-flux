<?php

namespace Bprs\AnalyticsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Logstate
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Bprs\AnalyticsBundle\Entity\LogstateRepository")
 */
class Logstate
{

    const BPRS_AN_REFERER   = "referer";
    const BPRS_AN_URL       = "url";
    const BPRS_AN_UNIQID    = "uniqID";
    const BPRS_AN_TIMESTAMP = "timestamp";
    const BPRS_AN_USERAGENT = "user_agent";
    const BPRS_AN_CLIENTIP  = "client_ip";
    const BPRS_AN_VALUES    = "info_values";

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="country", type="string", nullable=true)
     */
    private $country;

    /**
     * @ORM\Column(name="city", type="string", nullable=true)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="referer", type="string", length=2000, nullable=true)
     */
    private $referer;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=2000)
     */
    private $url;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestamp", type="datetime")
     */
    private $timestamp;

    /**
     * @var string
     *
     * @ORM\Column(name="user_agent", type="string", length=255)
     */
    private $userAgent;

    /**
     * @var string
     *
     * @ORM\Column(name="client_ip", type="string", length=128)
     */
    private $clientIp;

    /**
     * @ORM\Column(name="identifier", type="string", length=32, nullable=true)
     */
    private $identifier;

    /**
     * @ORM\Column(name="value", type="string", length=32, nullable=true)
     */
    private $value;

    /**
     * @ORM\Column(name="platform", type="string", length=32, nullable=true)
     */
    private $platform;

    /**
     * @ORM\Column(name="browser", type="string", length=32, nullable=true)
     */
    private $browser;

    /**
     * @ORM\Column(name="parent", type="string", length=50, nullable=true)
     */
    private $parent;

    /**
     * @ORM\Column(name="version", type="string", length=20, nullable=true)
     */
    private $version;

    /**
     * @ORM\Column(name="majorver", type="string", length=10, nullable=true)
     */
    private $majorver;

    /**
     * @ORM\Column(name="minorver", type="string", length=10, nullable=true)
     */
    private $minorver;

    /**
     * @ORM\Column(name="cssversion", type="string", length=10, nullable=true)
     */
    private $cssversion;

    /**
     * @ORM\Column(name="bot", type="boolean", nullable=true)
     */
    private $bot;

    /**
     * @ORM\Column(name="mobile", type="boolean", nullable=true)
     */
    private $mobile;

    public function __construct()
    {
        $this->timestamp = new \Datetime();
    }

    public function __toString()
    {
        return $this->url.' '.$this->Id;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set referer
     *
     * @param string $referer
     * @return Logstate
     */
    public function setReferer($referer)
    {
        $this->referer = $referer;

        return $this;
    }

    /**
     * Get referer
     *
     * @return string
     */
    public function getReferer()
    {
        return $this->referer;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Logstate
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set timestamp
     *
     * @param \DateTime $timestamp
     * @return Logstate
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    /**
     * Get timestamp
     *
     * @return \DateTime
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * Set userAgent
     *
     * @param string $userAgent
     * @return Logstate
     */
    public function setUserAgent($userAgent)
    {
        $this->userAgent = $userAgent;

        return $this;
    }

    /**
     * Get userAgent
     *
     * @return string
     */
    public function getUserAgent()
    {
        return $this->userAgent;
    }

    /**
     * Set clientIp
     *
     * @param string $clientIp
     * @return Logstate
     */
    public function setClientIp($clientIp)
    {
        $this->clientIp = $clientIp;

        return $this;
    }

    /**
     * Get clientIp
     *
     * @return string
     */
    public function getClientIp()
    {
        return $this->clientIp;
    }

    public function getIdentifier()
    {
        return $this->identifier;
    }

    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;
        return $this;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    public function setCountry($country)
    {
        $this->country = $country;
        return $this;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function getPlatform()
    {
        return $this->platform;
    }

    public function setPlatform($platform)
    {
        $this->platform = $platform;
        return $this;
    }

    public function getBrowser()
    {
        return $this->browser;
    }

    public function setBrowser($browser)
    {
        $this->browser = $browser;
        return $this;
    }

    /**
     * Set parent.
     *
     * @param string|null $parent
     *
     * @return Logstate
     */
    public function setParent($parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent.
     *
     * @return string|null
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set majorver.
     *
     * @param string|null $majorver
     *
     * @return Logstate
     */
    public function setMajorver($majorver = null)
    {
        $this->majorver = $majorver;

        return $this;
    }

    /**
     * Get majorver.
     *
     * @return string|null
     */
    public function getMajorver()
    {
        return $this->majorver;
    }

    /**
     * Set minorver.
     *
     * @param string|null $minorver
     *
     * @return Logstate
     */
    public function setMinorver($minorver = null)
    {
        $this->minorver = $minorver;

        return $this;
    }

    /**
     * Get minorver.
     *
     * @return string|null
     */
    public function getMinorver()
    {
        return $this->minorver;
    }

    /**
     * Set cssversion.
     *
     * @param string|null $cssversion
     *
     * @return Logstate
     */
    public function setCssversion($cssversion = null)
    {
        $this->cssversion = $cssversion;

        return $this;
    }

    /**
     * Get cssversion.
     *
     * @return string|null
     */
    public function getCssversion()
    {
        return $this->cssversion;
    }

    /**
     * Set bot.
     *
     * @param bool|null $bot
     *
     * @return Logstate
     */
    public function setBotInfo($bot = null)
    {
        $this->bot = preg_match('/bot|crawl|slurp|spider|mediapartners/i', $this->userAgent);

        return $this;
    }

    public function setBot($bot)
    {
        $this->bot = $bot;
        return $this;
    }

    /**
     * Get bot.
     *
     * @return bool|null
     */
    public function getBot()
    {
        return $this->bot;
    }

    public function getMobile()
    {
        return $this->mobile;
    }

    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
        return $this;
    }

    public function setMobileInfo($mobile = null)
    {
        $this->mobile = (
            preg_match(
                '/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',
                $this->userAgent
            ) ||
            preg_match(
                '/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',
                substr($this->userAgent,0,4)
            ));
    }

    /**
     * Set version.
     *
     * @param string|null $version
     *
     * @return Logstate
     */
    public function setVersion($version = null)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get version.
     *
     * @return string|null
     */
    public function getVersion()
    {
        return $this->version;
    }
}
