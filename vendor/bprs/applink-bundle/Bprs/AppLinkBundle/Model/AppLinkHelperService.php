<?php

namespace Bprs\AppLinkBundle\Model;

use Bprs\AppLinkeBundle\BprsAppLinkUrlInterface;

class AppLinkHelperService
{
    private $available_urls;

    public function __construct()
    {
        $this->available_urls = [];
    }

    public function getAvailableUrls($route_name = false)
    {
        if ($route_name) {
            if (array_key_exists($route_name, $this->available_urls)) {
                return $this->available_urls[$route_name];
            }
            return false;
        }
        return $this->available_urls;
    }

    /**
     * appends an array of route_name => "url" to available_urls.
     * Those urls are available with getAvailableUrls
     */
    public function addUrls($interface)
    {
        $urls = $interface->getUrls();
        foreach ($urls as $key => $value) {
            $this->available_urls[$key] = $value;
        }
    }
}
