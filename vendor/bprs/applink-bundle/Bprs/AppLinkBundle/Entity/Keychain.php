<?php

namespace Bprs\AppLinkBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Keychain
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Bprs\AppLinkBundle\Entity\Repository\KeychainRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Keychain implements UserInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Your owner of this keychain. can be a person, a bundle or whatever you need.
     * @var string
     *
     * @ORM\Column(name="user", type="string", length=30, unique=true)
     */
    private $user;

    /**
    * the security key.
    * @ORM\Column(name="api_key", type="string", length=32)
    */
    private $api_key;

    /**
    * the webadress to the bprsapplink api (remote)
    * @ORM\Column(name="url", type="string", length=100, unique=true)
    */
    private $url;

    /**
     * @var array
     * @ORM\OneToMany(targetEntity="Key", mappedBy="keychain")
     */
    private $keys;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /**
     * @var \DateTime
     * When this key was created
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     * When this key was last updated
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="uniqID", type="string", length=32)
     */
    private $uniqID;

    public function __construct() {
        $this->isActive = true;
        $this->keys = new ArrayCollection();
        $this->api_key= md5(openssl_random_pseudo_bytes(32));
        $this->uniqID = uniqID();
    }

    public function getUsername()
    {
        return $this->user;
    }

    public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        return null;
    }

    public function getPassword()
    {
        return $this->api_key;
    }

    public function getRoles()
    {
        $roles = array();
        foreach ($this->getKeys() as $key) {
            $roles[] = $key->getRole();
        }
        return $roles;
    }

    public function eraseCredentials()
    {

    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    public function getApiKey()
    {
        return $this->api_key;
    }

    public function setApiKey($api_key)
    {
        $this->api_key = $api_key;
        return $this;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    public function getKeys()
    {
        return $this->keys;
    }

    public function addKey($key)
    {
        $this->keys[] = $key;
        $key->setKeychain($this);
        return $this;
    }

    public function removeKey($key)
    {
        $this->keys->removeElement($key);
        return $this;
    }
    /**
     * Set createdAt
     * @ORM\PrePersist
     * @param \DateTime $createdAt
     * @return App
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();

        return $this;
    }

    /**
     * Get createdAt
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     * @ORM\PrePersist
     * @ORM\PreUpdate
     * @param \DateTime $updatedAt
     * @return App
     */
    public function setUpdatedAt()
    {
        $this->updatedAt = new \DateTime();

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function getUniqID()
    {
        return $this->uniqID;
    }

    public function setUniqID($uniqID)
    {
        $this->uniqID = $uniqID;
        return $this;
    }

    public function getIsActive()
    {
        return $this->isActive;
    }

    public function setIsActive($value)
    {
        $this->isActive = $value;
        return $this;
    }
}
