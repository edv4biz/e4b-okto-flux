<?php

namespace Bprs\AppLinkBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\Table;

class ListKeychainCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('bprs:applinkbundle:list_keys')
            ->setDescription('Lists all Keychains')
            ->addOption('role', false, InputOption::VALUE_REQUIRED, 'only list keychains with given role');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $keychains = null;
        if ($input->getOption('role')) {
        $keychains = $this->getContainer()->get('bprs_applink')->getKeychainsWithRole($input->getOption('role'));
    } else {
        $keychains = $this->getContainer()->get('bprs_applink')->getKeychains();
    }
        $table = new Table($output);
        $table->setHeaders(['uniqID', 'link']);

        $rows = [];
        if ($keychains) {
            foreach ($keychains as $keychain) {
                $rows[] = [$keychain->getUniqID(), $keychain->getUrl()];
            }
        }

        $table->setRows($rows);

        $table->render();
    }
}
