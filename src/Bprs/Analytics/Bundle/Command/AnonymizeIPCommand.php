<?php

namespace Bprs\AnalyticsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Security\Core\Encoder\BCryptPasswordEncoder;
use GeoIp2\Exception\AddressNotFoundException;
use GeoIp2\Exception\InvalidArgumentException;

class AnonymizeIPCommand extends ContainerAwareCommand {

    public function __construct()
    {
        parent::__construct();
    }

    protected function configure() {
        $this
            ->setName('bprs:analytics:anonymize_ips')
            ->setDescription('replaces readable ips with bcrypt hashed and salted strings and sets the country and city info if available');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $logstates = $em->getRepository('BprsAnalyticsBundle:Logstate')->getLogstatesToAnonymize(true)->iterate();

        $encoder = new BCryptPasswordEncoder(8);
        $salt = $this->getContainer()->getParameter('bprs_analytics.salt');
        $reader = $this->getContainer()->get('bprs_analytics_geoIP2_reader');
        $output->writeln('start anonymizing logstate ips');
        $counter = 0;
        foreach ($logstates as $row) {
            $logstate = $row[0];
            $encoded_ip = $encoder->encodePassword($logstate->getClientIp(), $salt);
            try {
                $record = $reader->city($logstate->getClientIp());

                $logstate->setCountry($record->country->isoCode);
                $logstate->setCity($record->city->name);
            } catch (AddressNotFoundException $e) {
            } catch (InvalidArgumentException $e) {
            } catch (Exception $e) {
            }
            $logstate->setClientIp($encoded_ip);
            $em->persist($logstate);
            $output->write('.');
            $counter++;
            if (($counter % 50) === 0) {
                $em->flush();
                $em->clear();
            }
        }
        $output->writeln(sprintf('finished anonymizing [%s] logstate ips', $counter));
    }
}
