<?php

namespace Bprs\AnalyticsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Bprs\AnalyticsBundle\Entity\Logstate;

/**
 * @Route("/bprs_analytics")
 */
class DefaultController extends Controller
{
    /**
     * @Route("/write_log", name="bprs_analytics_write_log")
     */
    public function writeLogstateAction(Request $request)
    {
        $this->get('bprs_analytics')->trackInfo($request, $request->get('identifier', null), $request->get('value', null));
        return new Response();
    }

    /**
     * @Route("/export", name="bprs_analytics_export")
     * @Security("is_granted('ROLE_BPRS_ANALYTICS_EXPORT')")
     * @Method("GET")
     */
    public function exportAction(Request $request)
    {
        $values = [];
        if ($request->query->get('identifier', false)) {
            $values['identifier'] = $request->query->get('identifier');
        }

        if ($request->query->get('value', false)) {
            $values['value'] = $request->query->get('value');
        }
        $logstates = $this->get('bprs_analytics')->getLogstatesInTime(
            $values,
            $request->query->get('start', '-1 day'),
            $request->query->get('end', 'now')
        );

        $delimiter = ';';
        $response = new StreamedResponse(function() use($logstates, $delimiter) {
            $handle = fopen('php://output', 'r+');
                fputcsv($handle,
                    [
                        'referer',
                        'url',
                        'identifier',
                        'value',
                        'country',
                        'city',
                        'timestamp',
                        'user-agent',
                        'platform',
                        'browser',
                        'parent',
                        'version',
                        'majorver',
                        'minorver',
                        'cssversion',
                        'mobile',
                        'bot'
                    ],
                    $delimiter
                );

            foreach ($logstates as $logstate) {
                fputcsv($handle,
                    [
                        $logstate->getReferer(),
                        $logstate->getUrl(),
                        $logstate->getIdentifier(),
                        $logstate->getValue(),
                        $logstate->getCountry(),
                        $logstate->getCity(),
                        $logstate->getTimestamp()->format('H:i d.m.Y'),
                        $logstate->getUserAgent(),
                        $logstate->getPlatform(),
                        $logstate->getBrowser(),
                        $logstate->getParent(),
                        $logstate->getVersion(),
                        $logstate->getMajorver(),
                        $logstate->getMinorver(),
                        $logstate->getCssversion(),
                        $logstate->getMobile() ? '1' : '0',
                        $logstate->getBot() ? '1' : '0'
                    ],
                    $delimiter
                );
            }
            fclose($handle);
        });

        $response->headers->set('Content-Type', 'application/force-download');
        $response->headers->set('Content-Disposition','attachment; filename="statistics.csv"');

        return $response;
    }
}
