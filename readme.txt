README

#### CHECKLISTE FÜR DAS ROLLOUT PROJEKT Okto.tv ####

Diese Liste soll bekannte Fehler und Probleme bei der Migration verhindern. Bitte aktualisieren!


1. Database-Table "role" in "db_role" umbenennen.
2. Überprüfung der Daten im ".env" File (vor allem bzgl. des Pfades für die Geo-Datenbank (*.mmdb)
3. Überprüfung der composer.json
4. htaccess-Routes überprüfen, vor allem die manuellen Workarounds sobald Prod auskommentieren und testen!